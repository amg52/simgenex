from numpy import matrix, diag, sqrt

# mahalanobis distance
def rkm(x, xk, CI, nvar):
    if nvar == 2:
        wts = matrix(diag((2./3., 1./3.)))
    elif nvar == 3:
        wts = matrix(diag((2./3., 1./6., 1./6.)))
    xm  = matrix(x)                          # = Euclidean for C = I
    xkm = matrix(xk)
    d = xm - xkm                             # x is like (40, 3), xk (1, 3)
    dm = d * wts * CI * wts.T * d.T          # row vector * CI * col vector 
    val = sqrt(diag(dm))                     # weighted distance vector
    return val