# Adapted for numpy/ma/cdms2 by convertcdms.py
"""
Detrend a time series

Usage: xdt,tline,coeffs = detrend.dt(x), where

x is the time series
xdt is the detrended series
tline is the trend line (that was subtracted from the series)
coeffs is an array containing the line coefficients

"""

import numpy as np
from scipy.stats import linregress as lm

#
def dt(y):
  ly = len(y)
  x = np.arange(ly)
  fm = lm(x,y)
  coeffs = fm[:2]
  line = fm[1] + fm[0]*x
  
  xdt = y-line
  
#  print 'coeffs contains slope, then intercept!'
  
  return xdt,line,coeffs

#############
