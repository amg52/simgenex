"""

Recon(struct) a trivariate data series from PCs and EOFs, where the data
has been both standardized and area-weighted (using makebigarray.py). The
mean and std arrays are available, also latitudes... which should be enough.

"""

from numpy import cos, pi, resize, sqrt, zeros
from numpy.ma import masked_greater

def r1(eof, pc, var, neof, meanar, stdar, lats):
    """ Need to unwind the input sequence: standardization followed by
    area-weighting, followed by PCA. So first we reconstruct the
    series (using neof EOFs and PCs), then area-unweight and then
    unstandardize. The lats array is for unweighting, meanar and stdar
    for unstandardizing. var is the variable name, used to retrieve
    the correct slice from meanar and stdar."""

# First, (re)construct the 3-D time series
    npc, nrow, ncol = eof.shape                # For SESA like 10, 30, 40
    lpc = len(pc)                              # like 108 (1901-2008)
    recon0 = zeros((lpc, nrow, ncol))

# Reconstructing . . .
    for k in range(neof):
        # add contributions from the neofs eofs
        recon0 += eof[k] * resize(pc[:, k], (ncol, nrow, lpc)).T

# Next, area-unweighting
    recon1 = unwt(recon0, lats)

# Now, unstandardize
    vardic = {'pr': 0, 'tmx': 1, 'tmn': 2}
    arrmean = meanar[vardic[var]]; arrstd = stdar[vardic[var]]
    tmp = recon1 * arrstd + arrmean       # Must be pr array for pr, etc.
    return masked_greater(tmp, 1e10)

###########

def r3(eofdic, pc, neof, meanar, stdar, lats):
    """ Let's do all three variables (pr, tmx, tmn) at once!  Note:
    eofdic must have keys 'pr', 'tmx' and 'tmn', corresponding to the
    respective arrays."""
    vars = ('pr','tmx','tmn')
    ct = 0
    recondic = {}
    for i in vars:
        recondic[i] = r1(eofdic[i], pc, vars[ct], neof, meanar, stdar, lats)
    return recondic

###########

def unwt(x, lats):
    sm = x.shape
    sm21 = (sm[2], sm[1])
    wts0 = cos(pi * lats / 180.)
    wts0[lats == 90] = 0.
    wts1 = sqrt(wts0)
    wts2 = len(lats) * wts1 / wts1.sum()       # Sum = orig sum
    wts2inv = 1. / wts2                        # Undo area-weighting
    wts3 = resize(wts2inv, sm21).T             # (lat,lon) shape
    wtsre = resize(wts3, sm)                   # Same for every time step
    return wtsre * x

############