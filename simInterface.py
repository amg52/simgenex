# builtin
import cPickle as pickle
from netCDF4 import Dataset as nc
from numpy import loadtxt, ones, log, where, zeros, arange
    
def checkParams(p, requiredParams, requiredTypes):
    for i in range(len(requiredParams)):
        r = requiredParams[i]
        if not r in p.keys():
            e = 'simInterface.checkParams: \
            Parameter {:s} not found in parameters'.format(r)
            raise Exception(e)
        pi = p[r]
        if requiredTypes[i] == 'num':
            if not isinstance(pi, float) and not isinstance(pi, int):
                e = 'simInterface.checkParams: \
                Parameter {:s} must be number'.format(r)
                raise Exception(e)
        elif requiredTypes[i] == 'int':
            if not isinstance(pi, int):
                e = 'simInterface.checkParams: \
                Parameter {:s} must be integer'.format(r)
                raise Exception(e)
        elif requiredTypes[i] == 'bool':
            if not isinstance(pi, bool):
                e = 'simInterface.checkParams: \
                Parameter {:s} must be boolean'.format(r)
                raise Exception(e)
        elif requiredTypes[i] == 'str':
            if not isinstance(pi, str):
                e = 'simInterface.checkParams: \
                Parameter {:s} must be string'.format(r)
                raise Exception(e)
        elif requiredTypes[i] == 'list':
            if not isinstance(pi, list):
                e = 'simInterface.checkParams: \
                Paramter {:s} must be list'.format(r)
                raise Exception(e)
        else:
            raise Exception('simInterface.checkParams: Unrecognized type')
    return p

class Out(object):
    def __init__(self):
        # resampling indices
        self.jixar20 = []
        self.jixar21 = []
        # regional
        self.prTrReg = []
        self.maTrReg = []
        self.miTrReg = []
        self.prVarReg = []
        self.maVarReg = []
        self.miVarReg = []
        self.prTotReg = []
        self.maTotReg = []
        self.miTotReg = []
        # gridbox-level
        self.prTrAr = []
        self.maTrAr = []
        self.miTrAr = []
        self.prVarAr = []
        self.maVarAr = []
        self.miVarAr = []
        self.prTotAr = []
        self.maTotAr = []
        self.miTotAr = []
        # average
        self.prTrAve = []
        self.maTrAve = []
        self.miTrAve = []
        self.prVarAve = []
        self.maVarAve = []
        self.miVarAve = []
        self.prTotAve = []
        self.maTotAve = []
        self.miTotAve = []
        # all
        self.prAll = []
        self.maAll = []
        self.miAll = []
        # time     
        self.time = []

class In(object):
    # static member variable
    __mths__ = {'Jan': 0, 'Feb': 1, 'Mar': 2, 'Apr': 3, \
        'May': 4, 'Jun': 5, 'Jul': 6, 'Aug': 7, \
        'Sep': 8, 'Oct': 9, 'Nov': 10, 'Dec': 11}
    
    def __init__(self, p):
        if p == {}: raise Exception('simInterface.In.init:\
        Parameter dictionary is empty')
        
        # error checking
        requiredParams = ['seas', 'bfactor', 'obsStartYear', 'obsStartYear',\
                          'simlen', 'locate', 'nnn', 'rngSeed', 'xval',\
                          'trendMethod', 'scale', 'perPrChange', 'dirname',\
                          'outputAppend', 'write', 'simix', 'pcsFile',\
                        'totalGriddedObsFile', 'totalGriddedDetrendedObsFile',\
                          'totalYearlyObsFile', 'totalYearlyDetrendedObsFile',\
                          'prTmpRegFile', 'prOzoneRegFile', 'tmmmFile',\
                          'prAllMosFile', 'tmaxAllMosFile','tminAllMosFile',\
                          'prClimDicFile', 'tmaxClimDicFile',\
                          'tminClimDicFile', 'eofsFile', 'yearsFile']
        requiredTypes = ['list', 'num', 'int', 'int', 'int', \
            'int', 'int', 'int', 'int', 'str', \
            'bool', 'num', 'str', 'str', 'bool', 'int', \
            'str', 'str', 'str', 'str', 'str', \
            'str', 'str', 'str', 'str', \
            'str', 'str', 'str', 'str', \
            'str', 'str', 'str']
        if p['reg'] =='WA':
            requiredParams.append('PrSmoothedFile')
            requiredTypes.append('str')
        else:
            pass
        
        p = checkParams(p, requiredParams, requiredTypes)
        
        # miscellaneous value checking
        if p['xval']:
            minval = p['locate'] - p['obsStartYear'] + 1
        else:
            minval = p['locate'] - p['obsEndYear'] - 1
        if p['simix'] < minval:
            e = 'simInterface.In.init: simix cannot be less than {:d}'.format(minval)
            raise Exception(e)
        if p['trendMethod'] != 'Linear' and p['trendMethod'] != 'Log':
            raise Exception('simInterface.In.init: Unrecognized trend method')
        if p['perPrChange'] < 0 or p['perPrChange'] > 1:
            raise Exception('simInterface.In.init: Forced precipitation percent must lie between 0 and 1')
        if p['seas'] == []:
            raise Exception('simInterface.In.init: Season list must be nonempty')

        # save parameters
        self.__p = p

        # compute beginning and ending indices
        years = self.loadObsYears()
        if self.obsStartYear() < min(years):
            raise Exception('simInterface.In.init: Start date out of range')
        if self.obsEndYear() > max(years):
            raise Exception('simInterface.In.init: End date out of range')
        startIdx = where(years == self.obsStartYear())[0]
        if not startIdx.size:
            raise Exception('simInterface.In.init: Start year not found')
        endIdx = where(years == self.obsEndYear())[0]
        if not endIdx.size:
            raise Exception('simInterface.In.init: End year not found')
        self.__begix = startIdx[0]
        self.__endix = endIdx[0] + 1

    def loadPCs(self):
        pcs = loadtxt(self.__p['pcsFile'])
        if not self.xval():
            # ignores observational period
            simstart = self.simix() + (self.simStartYear() - self.locate())
            simend = simstart + self.simlen()
        else:
            # includes observational period
            simstart = self.simix() + (self.simStartYear() - self.locate() - self.obslen())
            simend = simstart + (self.obslen() + self.simlen())
        return pcs[simstart : simend, :]

    def loadEOFs(self): return [pickle.load(open(self.__p['eofsFile']))[i] for i in [0, 1, 2, 6, 7, 8, 9]]        
                        
    def loadGriddedObservations(self):
        pr, tmax, tmin = pickle.load(open(self.__p['totalGriddedObsFile']))
        pr = pr[self.begix() : self.endix()]
        tmax = tmax[self.begix() : self.endix()]
        tmin = tmin[self.begix() : self.endix()]
        return pr, tmax, tmin
        
    def loadGriddedDetrendedObservations(self):
        pr, tmax, tmin = pickle.load(open(self.__p['totalGriddedDetrendedObsFile']))
        pr = pr[self.begix() : self.endix()]
        tmax = tmax[self.begix() : self.endix()]
        tmin = tmin[self.begix() : self.endix()]
        return pr, tmax, tmin
        
    def loadYearlyObservations(self):
        raw = loadtxt(self.__p['totalYearlyObsFile'])
        return raw[self.begix() : self.endix(), :]
        
    def loadYearlyDetrendedObservations(self):
        rawdt = loadtxt(self.__p['totalYearlyDetrendedObsFile'])
        return rawdt[self.begix() : self.endix(), :]

    def loadPrecipTrend(self):
        # compute precipitation trend by regressing precipitation on global 
        # mean temperature and ozone separately and combining using bfactor
        tmp = loadtxt(self.__p['prTmpRegFile'])
        ozone = loadtxt(self.__p['prOzoneRegFile'])
        pr = self.bfactor() * tmp + (1 - self.bfactor()) * ozone
        pr20forced = pr[self.begix() : self.endix()]
        pr21forced = pr[self.endix() : self.endix() + self.simlen()]
        prforced = pr[self.begix() : self.endix() + self.simlen()]
        return pr20forced, pr21forced, prforced
        
    def loadPrecipSmoothed(self):
       prSmoothed = loadtxt(self.__p['prSmoothedFile'])
       return prSmoothed[self.begix() : self.endix()]
        
    def loadMultimodelTemp(self):
        tmmm = pickle.load(open(self.__p['tmmmFile']))
        tmmm -= tmmm.mean()
        tmmmYears = range(1901, 1901 + len(tmmm)) # starts in year 1901
        start = tmmmYears.index(self.obsStartYear())
        end = tmmmYears.index(self.obsEndYear()) + 1   
        obsTmmm = tmmm[start : end]
        simTmmm = tmmm[end : end + self.simlen()]
        allTmmm = tmmm[start : end + self.simlen()]
        return obsTmmm, simTmmm, allTmmm
        
    def loadLatLonGrid(self):
        fPr = nc(self.__p['prAllMosFile'])
        lats = fPr.variables['lat'][:].astype(float) # convert to float
        lons = fPr.variables['lon'][:].astype(float)
        fPr.close()
        return lats, lons
            
    def loadTimeUnits(self):
        fPr = nc(self.__p['prAllMosFile'])
        units = fPr.variables['time'].units
        fPr.close()
        return units
        
    def loadObsYears(self): return pickle.load(open(self.__p['yearsFile']))
    
    def loadYears(self):
        obsYears = self.loadObsYears()[self.begix() : self.endix()]
        simYears = arange(self.simStartYear(), self.simStartYear() + self.simlen())
        allYears = zeros((self.obslen() + self.simlen(),))
        allYears[: self.obslen()] = obsYears
        allYears[self.obslen() :] = simYears
        return obsYears, simYears, allYears

    def loadClimatology(self):
        obslen = self.obslen()
        begix = self.begix()
        endix = self.endix()
        
        # load data for all months
        fPr = nc(self.__p['prAllMosFile'])
        fTmax = nc(self.__p['tmaxAllMosFile'])
        fTmin = nc(self.__p['tminAllMosFile'])
        prAll = fPr.variables['pr'][:]       # NOT! Already converted # / 30
        tmaxAll = fTmax.variables['tmx'][:]                
        tminAll = fTmin.variables['tmn'][:]
        fPr.close()
        fTmax.close()
        fTmin.close()
        
        # load climatological dictionaries
        prDic = pickle.load(open(self.__p['prClimDicFile']))    
        tmaxDic = pickle.load(open(self.__p['tmaxClimDicFile']))
        tminDic = pickle.load(open(self.__p['tminClimDicFile']))
        
        newmolen = 12 * (obslen + self.simlen()) # 12 months per year
        nlats = prAll.shape[1]
        nlons = prAll.shape[2]
        climmos = list(set(In.__mths__.keys()) - set(self.seas()))

        # fill in select months  
        prSim = 1e20 * ones((newmolen, nlats, nlons), 'f')
        tmaxSim = 1e20 * ones((newmolen, nlats, nlons), 'f')
        tminSim = 1e20 * ones((newmolen, nlats, nlons), 'f')
        simstart = 0
        if not self.xval():
            # fill in ALL months in observational period?
            prSim[: 12 * obslen] = prAll[12 * begix : 12 * endix]
            tmaxSim[: 12 * obslen] = tmaxAll[12 * begix : 12 * endix]
            tminSim[: 12 * obslen] = tminAll[12 * begix : 12 * endix]
            # change start of simulation
            simstart = obslen
        for i in range(simstart, obslen + self.simlen()):
            for j in range(len(climmos)):
                mth = climmos[j]
                prSim[12 * i + In.__mths__[mth]] = prDic[mth]
                tmaxSim[12 * i + In.__mths__[mth]] = tmaxDic[mth]
                tminSim[12 * i + In.__mths__[mth]] = tminDic[mth]

        return prAll, tmaxAll, tminAll, prSim, tmaxSim, tminSim

    # get and utility functions
    def begix(self): return self.__begix
    def endix(self): return self.__endix
    def nnn(self): return self.__p['nnn']
    def xval(self): return self.__p['xval']
    def seas(self): return self.__p['seas']
    def simix(self): return self.__p['simix']
    def write(self): return self.__p['write']
    def scale(self): return self.__p['scale']
    def locate(self): return self.__p['locate']
    def simlen(self): return self.__p['simlen']
    def bfactor(self): return self.__p['bfactor']
    def dirname(self): return self.__p['dirname']
    def rngSeed(self): return self.__p['rngSeed']
    def obsEndYear(self): return self.__p['obsEndYear']
    def simStartYear(self): return self.obsEndYear() + 1
    def trendMethod(self): return self.__p['trendMethod']
    def obsStartYear(self): return self.__p['obsStartYear']
    def outputAppend(self): return self.__p['outputAppend']
    def perPrChange(self): return log(1 + self.__p['perPrChange'])
    def obslen(self): return self.simStartYear() - self.obsStartYear()
    def seasStr(self): return ''.join([m[0] for m in self.__p['seas']])
