"""

Pairwise correlation coeffs...

Call: cor0, cor1 = comp(arr0, arr1, lats=None, thin=5), where

arr0 and arr1 might represent probs, prsim, e.g., and

thin is a stride variable. Otherwise, like for SESA, we end up with plots
so dense they can't be read.

"""

from numpy import corrcoef as cc, ones
import map2mat

def comp(arr0, arr1, lats=None, thin=5):
    """ arr0, arr1 are T, Y, X. This step should squeeze out masked grids. """
    arrmat0 = map2mat.m2mat(arr0, lats)
    arrmat1 = map2mat.m2mat(arr1, lats)
    cor0 = cc(arrmat0.T)
    cor1 = cc(arrmat1.T)

# Now, want to retain only elements above (not on) main diagonal...

    sc = cor0.shape
    dim = sc[0]
    corflat0 = 1e20*ones(dim * (dim - 1) / 2)
    corflat1 = 1e20*ones(dim * (dim - 1) / 2)

    ct = 0
    for i in range(0, dim, thin):
        for j in range(i+1, dim):
            corflat0[ct] = cor0[i, j]
            corflat1[ct] = cor1[i, j]
            ct += 1

    print 'Inserted', ct, 'data!'

    return corflat0[:ct], corflat1[:ct]


#############
