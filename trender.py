# custom
import simInterface as intf
# builtin
import abc
from numpy.ma import masked_greater
from scipy.stats import linregress as lm
from numpy import ones, zeros, pi, cos, resize, log, exp

# trend base class
class GridTrender(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self, p = {}):
        self.input = intf.In(p)
        # load only data that are needed
        self.obsTmmm, self.allTmmm = self.input.loadMultimodelTemp()[0 :: 2]
        self.coef0, self.coef1 = self.input.loadEOFs()[5 : 7]
        self.obsSeasPr = self.input.loadGriddedObservations()[0]
        self.raw = self.input.loadYearlyObservations()
        # commonly used quantities
        self.obslen = self.input.obslen()
        self.simlen = self.input.simlen()
        self.nlats = self.obsSeasPr.shape[1]
        self.nlons = self.obsSeasPr.shape[2]
     
    def gridboxTrends(self):                 
        pr = self.gridboxPr()
        xmmar = 1e20 * ones((self.obslen + self.simlen, self.nlats, self.nlons))
        for j in range(self.nlats):
            for k in range(self.nlons):
                xmmar[:, j, k] = self.allTmmm
        ma = self.coef0[1] * xmmar + self.coef1[1]
        mi = self.coef0[2] * xmmar + self.coef1[2]
        return pr, ma, mi
        
    def regionalTrends(self):
        pr = self.regionalPr()
        fmma = lm(self.obsTmmm, self.raw[:, 1])
        fmmi = lm(self.obsTmmm, self.raw[:, 2])
        ma = fmma[1] + fmma[0] * self.allTmmm
        mi = fmmi[1] + fmmi[0] * self.allTmmm
        return pr, ma, mi
    
    @abc.abstractmethod 
    def gridboxPr(self):
        # broadcast regional precipitation trend to individual gridboxes
        return
        
    @abc.abstractmethod
    def regionalPr(self):
        # calculate regional precipitation trend
        return

# linear regression trend class
class LinearGridTrender(GridTrender):
    def __init__(self, p = {}):
        super(LinearGridTrender, self).__init__(p)
        # load only data that are needed
        self.prforced = self.input.loadPrecipTrend()[2]
        self.lats = self.input.loadLatLonGrid()[0]

    # implementation of abstract methods
    def gridboxPr(self): 
        # get regional precipitation trend
        prforced = self.regionalPr()
        
        # used for scaling correction
        if self.input.scale():
            scaling = zeros(self.obslen + self.simlen)
            wts = cos(pi * self.lats / 180) 
            B0 = 0
            B1 = 0
            N = 0
            
        # regress individual gridboxes
        pr = 1e20 * ones((self.obslen + self.simlen, self.nlats, self.nlons))
        for i in range(self.nlats):
            for j in range(self.nlons):
                if not self.obsSeasPr.mask[0][i, j]:
                    fmtr = lm(prforced[: self.obslen], self.obsSeasPr[:, i, j])
                    pr[:, i, j] = fmtr[0] * prforced + fmtr[1]
                    if self.input.scale():
                        B0 += wts[i] * fmtr[1]
                        B1 += wts[i] * fmtr[0]
                        N += wts[i]
        pr = masked_greater(pr, 1e10) # mask
        if not self.input.scale(): return pr
          
        # compute correction
        scaling = N * prforced / (B1 * prforced + B0)
        # apply correction
        return pr * resize(scaling, (self.nlons, self.nlats, len(scaling))).T
    
    def regionalPr(self): return self.prforced
        
# log regression trend class
class LogGridTrender(GridTrender):
    def __init__(self, p = {}):
        super(LogGridTrender, self).__init__(p)
        # load only data that are needed
        self.prSmoothed = self.input.loadPrecipSmoothed()
        self.simTmmm = self.input.loadMultimodelTemp()[1]

    # implementation of abstract methods
    def gridboxPr(self):
        pr = 1e20 * ones((self.obslen + self.simlen, self.nlats, self.nlons))
        # regress individual gridboxes
        impChange = self.input.perPrChange()
        for i in range(self.nlats):
            for j in range(self.nlons):
                if not self.obsSeasPr.mask[0][i, j]:
                    # regress
                    fm20 = lm(self.prSmoothed, self.obsSeasPr[:, i, j])
                    pr[: self.obslen, i, j] = fm20[0] * self.prSmoothed + fm20[1]
                    # add simulated percent precipitation trend to end
                    offset = log(pr[self.obslen - 1, i, j]) - impChange * self.simTmmm[0]
                    pr[self.obslen :, i, j] = exp(impChange * self.simTmmm + offset)
        return masked_greater(pr, 1e10) # mask
        
    def regionalPr(self):
        prforced = 1e20 * ones((self.obslen + self.simlen,))
        # regress
        fm20 = lm(self.prSmoothed, self.raw[:, 0])
        prforced[: self.obslen] = fm20[0] * self.prSmoothed + fm20[1]
        # add simulated percent precipitation trend to end
        impChange = self.input.perPrChange()
        offset = log(prforced[self.obslen - 1]) - impChange * self.simTmmm[0]
        prforced[self.obslen :] = exp(impChange * self.simTmmm + offset)
        return prforced
