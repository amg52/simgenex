"""

Quick pickling and unpickling. If a directory named 'pickled' exists
within the directory in which this script is invoked the .p file will 
be saved there; otherwise it is saved to the working directory.

The script appends ".p" so this should not be done when naming files.

Feb 2009: New functions dd,dl,ddic,ldic,pack,reassemble added, for
preserving axes while pickling. These still need work as of
02/07/2009...

"""

import cPickle as pickle
import os

dlist = os.listdir('.')

def d(x,name):
    if 'pickled' in dlist:
        fullname = 'pickled/'+name+'.p'
    else:
        fullname = name+'.p'
    pickle.dump(x,open(fullname,'w'))
    print 'Dumped to', fullname

######################

def l(name):
    if 'pickled' in dlist:
        fullname = 'pickled/'+name+'.p'
    else:
        fullname = name+'.p'
    x = pickle.load(open(fullname))
    return x

######################
    
def ddic(x,name):
    """ For pickling dictionaries, not individual files! """
    xk=x.keys()
    pdic={}
    for i in xk:
        pdic[i]=pack(x[i])

    if 'pickled' in dlist:
        fullname='pickled/'+name+'_dic.p'
        pickle.dump(pdic,open(fullname,'w'))
        print 'Dumped to', fullname
    else:
        fullname=name+'_dic.p'
        pickle.dump(pdic,open(fullname,'w'))
        print 'Dumped to', fullname

################################

