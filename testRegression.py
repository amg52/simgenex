from numpy.random import rand
import matplotlib.pyplot as plt
from aav import av as areaAverage
from scipy.stats import linregress as lm
from numpy import arange, log, exp, array, zeros

npts = 100
logRegress = True
averageMethod = 'Area'
lats = array([10, 10.5])

# independent variable
x = arange(npts)

# generate two sequences of random data
data = zeros((npts, 2, 1))
y1 = 5 + 10 * rand(npts)
y2 = 10 + 10 * rand(npts)
data[:, 0, 0] = y1
data[:, 1, 0] = y2

# average data
if averageMethod == 'Area':
    ym = areaAverage(data, lats)
else:
    ym = (y1 + y2) / 2

# regress individual
if logRegress:
    coef1 = lm(x, log(y1))
    coef2 = lm(x, log(y2))
    yhat1 = exp(coef1[0] * x + coef1[1])
    yhat2 = exp(coef2[0] * x + coef2[1])
else:
    coef1 = lm(x, y1)
    coef2 = lm(x, y2)
    yhat1 = coef1[0] * x + coef1[1]
    yhat2 = coef2[0] * x + coef2[1]

# average regression
if averageMethod == 'Area':
    datahat = zeros((npts, 2, 1))
    datahat[:, 0, 0] = yhat1
    datahat[:, 1, 0] = yhat2
    yhatAve = areaAverage(datahat, lats)
else:
    yhatAve = (yhat1 + yhat2) / 2

# regress mean
if logRegress:
    coefm = lm(x, log(ym))
    yhatm = exp(coefm[0] * x + coefm[1])
else:
    coefm = lm(x, ym)
    yhatm = coefm[0] * x + coefm[1]

# plot
plt.figure(1)
plt.clf()
plt.plot(x, yhatAve, 'r-', label = 'Regress + Average')
plt.plot(x, yhatm, 'g-', label = 'Average + Regress')
plt.plot(x, y1, 'kx', label = 'Series 1')
plt.plot(x, y2, 'mx', label = 'Series 2')
plt.plot(x, yhat1, 'k-', label = 'Regression 1')
plt.plot(x, yhat2, 'm-', label = 'Regression 2')
plt.xlabel('X')
plt.ylabel('Y')
plt.legend()
plt.show()