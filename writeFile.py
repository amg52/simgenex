import ncwrite
import numpy as np

def gendate(k, simstartyr):
    mos = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12']
    outl = []

    if k / 12 != k / 12.:
        print 'Number of months not divisible by 12!'
        return 'xxxx-xxx-xxx'
    else:
        nyrs = int(k / 12.)                    
        for i in range(nyrs):
            yr = simstartyr + i
            for j in range(12):
                mo = mos[j]
                ix = 12 * i + j
                outl.append(str(yr) + '-' + mo + '-01')

    return outl

def writefile(lats, lons, nvar, tunitsallmos, simix, begix, endix, seas, bfactor, \
    simlen, dirname, prsimm, masimm, misimm):
    begix += 1
    endix -= 1
    
    froot = '_TS3.1or10_25-40S_45-65W'
    taxfull = np.arange(-707.5, 1632.5)
    taxsim = taxfull[12 * begix : 12 * (endix + simlen)]
    fname = dirname + '/simall' + '_' + seas + '_%4.2f' % bfactor + \
            '_' + str(simix).zfill(5) + froot
    ncwrite.w({'pre' : prsimm, 'tmx': masimm, 'tmn': misimm}, \
        {'pre': 'mm/d', 'tmx': 'deg C', 'tmn': 'deg C'}, \
        lats, lons, taxsim, tunitsallmos, fname)

    return

#    if UY:
#        latsUY = lats[10:20]
#        lonsUY = lons[12:24]
#        prUY = prsimm[12:,10:20,12:24]
#        if nvar == 2:
#            mnUY = mnsimm[12:,10:20,12:24]
#        elif nvar == 3:
#            maUY = masimm[12:,10:20,12:24]
#            miUY = misimm[12:,10:20,12:24]
#        suy = prUY.shape
#        froot = seas+'_%4.2f'%(bfactor)+'_'+str(simix).zfill(5)+\
#                '_TS3.1_UY.dat'
#        print 'Opening UY files:'
#        fnamepr = dirname+'/simpre'+froot
#        print fnamepr
#        fobjpr = open(fnamepr,'w')
#        if nvar == 2:
#            fnamemn = dirname+'/simtmp'+froot
#            print fnamemn
#            fobjmn = open(fnamemn,'w')
#        elif nvar == 3:
#            fnamema = dirname+'/simtmx'+froot
#            fnamemi = dirname+'/simtmn'+froot
#            print fnamema
#            print fnamemi
#            fobjma = open(fnamema,'w')
#            fobjmi = open(fnamemi,'w')
#
#        datelist = gendate(suy[0], obsstartyear + 1)
#        firstrowlist  = ['Latitudes ']
#        secondrowlist = ['Longitudes']
#
#        # first write the header rows
#        print 'Writing headers...' 
#        for i in range(len(latsUY)):
#            for j in range(len(lonsUY)):
#                if not prUY.mask[0,i,j]:
#                    firstrowlist.append('%8.2f'%(latsUY[i]))
#                    secondrowlist.append('%8.2f'%(lonsUY[j]))
#        firstrowstr  = ' '.join(firstrowlist)+'\n'
#        secondrowstr = ' '.join(secondrowlist)+'\n'
#        fobjpr.write(firstrowstr)
#        fobjpr.write(secondrowstr)
#        if nvar == 2:
#            fobjmn.write(firstrowstr)
#            fobjmn.write(secondrowstr)
#        elif nvar == 3:
#            fobjma.write(firstrowstr)
#            fobjma.write(secondrowstr)
#            fobjmi.write(firstrowstr)
#            fobjmi.write(secondrowstr)
#
#        # then the data...
#        print 'Writing data...'
#        for k in range(suy[0]):              # One row at a time
#            if k/100 == k/100.:
#                print 'Writing row', k
#            rowlistpr = [datelist[k]]
#            if nvar == 2:
#                rowlistmn = [datelist[k]]
#            elif nvar == 3:
#                rowlistma = [datelist[k]]
#                rowlistmi = [datelist[k]]
#            for i in range(len(latsUY)):
#                for j in range(len(lonsUY)):
#                    if not prUY.mask[0,i,j]:
#                        rowlistpr.append('%8.2f'%(prUY[k,i,j]))
#                        if nvar == 2:
#                            rowlistmn.append('%8.2f'%(mnUY[k,i,j]))
#                        elif nvar == 3:
#                            rowlistma.append('%8.2f'%(maUY[k,i,j]))
#                            rowlistmi.append('%8.2f'%(miUY[k,i,j]))
#            rowstrpr = ' '.join(rowlistpr)+'\n'
#            fobjpr.write(rowstrpr)
#            if nvar == 2:
#                rowstrmn = ' '.join(rowlistmn)+'\n'
#                fobjmn.write(rowstrmn)
#            elif nvar == 3:
#                rowstrma = ' '.join(rowlistma)+'\n'
#                rowstrmi = ' '.join(rowlistmi)+'\n'
#                fobjma.write(rowstrma)
#                fobjmi.write(rowstrmi)
#
#        print 'Closing UY files...'
#        fobjpr.close()
#        if nvar == 2:
#            fobjmn.close()
#        elif nvar == 3:
#            fobjma.close()
#            fobjmi.close()
#
#    if nvar == 2:
#        return prsimm, mnsimm, prforced[begix:endix+simlen]
#    elif nvar == 3:
#        return prsimm, masimm, misimm, prforced[begix:endix+simlen]