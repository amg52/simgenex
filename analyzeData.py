# import and call startup script
import startup
startup

# builtin
from sim import SimFile
from recon import r1 as rec
from createMap import createMap
from aav import av as areaAverage
# custom
import numpy.ma as ma
from matplotlib.pyplot import *
import matplotlib.gridspec as gridspec
from numpy import corrcoef, array, where, linspace, sqrt, resize

# ==========
# RUN SIMGEN
# ==========
params = 'params/WA_000.txt'
s = SimFile(params)
s.run()

# =============
# GET VARIABLES
# =============

obsYears, allYears = s.input.loadYears()[0 :: 2]
obsGriddedPr, obsGriddedTmax, obsGriddedTmin = s.input.loadGriddedObservations()
obsGriddedDtPr, obsGriddedDtTmax, obsGriddedDtTmin = s.input.loadGriddedDetrendedObservations()
pcs = s.input.loadPCs()
eofPr, eofTmax, eofTmin, meanArray, stdArray, coef0, coef1 = s.input.loadEOFs()
obs = s.input.loadYearlyObservations()
obsdt = s.input.loadYearlyDetrendedObservations()
tmmm = s.input.loadMultimodelTemp()[0]
prSmoothed = s.input.loadPrecipSmoothed()
lats, lons = s.input.loadLatLonGrid()
nPCs = pcs.shape[1]
prrecon = rec(eofPr, pcs, 'pr', nPCs, meanArray, stdArray, lats)
marecon = rec(eofTmax, pcs, 'tmx', nPCs, meanArray, stdArray, lats)
mirecon = rec(eofTmin, pcs, 'tmn', nPCs, meanArray, stdArray, lats)
idx = where(allYears == s.input.locate())[0][0] # where unique decade is located

# =======
# OPTIONS
# =======

lenLat = lats[-1] - lats[0]
lenLon = lons[-1] - lons[0]
lat1 = where(lats > lats[0] + lenLat / 3)[0][0]
lat2 = where(lats > lats[0] + lenLat * 2 / 3)[0][0]
lon1 = where(lons > lons[0] + lenLon / 3)[0][0]
lon2 = where(lons > lons[0] + lenLon * 2 / 3)[0][0]
latsToPlot = [lat1, lat1, lat2, lat2]
lonsToPlot = [lon1, lon2, lon1, lon2]
numPlots = 1

# ====
# PLOT
# ====

# xticks
xt = range(int(min(allYears)), int(max(allYears)), 20)
xt2 = range(int(min(obsYears)), int(max(obsYears)), 20)

# three-panel plot of trended regional pr, tmax, tmin
fig = figure(numPlots, figsize = (15, 10))
gs = gridspec.GridSpec(3, 2, width_ratios = [4, 1])
# pr
subplot(gs[0])
plot(obsYears, obs[:, 0], 'b-', label = 'Obs Tot')
plot(allYears, s.output.prTotAve, 'r-', label = 'Sim Tot Ave')
plot(allYears, s.output.prTrAve, 'k--', label = 'Sim Trend Ave')
plot(allYears, s.output.prTotReg, 'g', label = 'Sim Tot Reg')
plot(allYears, s.output.prTrReg, 'm--', label = 'Sim Trend Reg')
plot([allYears[idx], allYears[idx]], ylim(), 'k', lw = 2)
plot([allYears[idx] + 9, allYears[idx] + 9], ylim(), 'k', lw = 2)
plot([max(obsYears), max(obsYears)], ylim(), 'k--', lw = 2)
xlabel('Year')
ylabel('Pr (mm/d)')
xlim(min(allYears), max(allYears))
xticks(xt)
title('Trended Regional -- Decade: [' + str(allYears[idx]) + ', ' + str(allYears[idx + 9]) + ']')
legend(bbox_to_anchor = (1.05, 1), loc = 2, borderaxespad = 0.)
grid()
# tmax
subplot(gs[2])
plot(obsYears, obs[:, 1], 'b-', label = 'Obs Tot')
plot(allYears, s.output.maTotAve, 'r-', label = 'Sim Tot Ave')
plot(allYears, s.output.maTrAve, 'k--', label = 'Sim Trend Ave')
plot(allYears, s.output.maTotReg, 'g', label = 'Sim Tot Reg')
plot(allYears, s.output.maTrReg, 'm--', label = 'Sim Trend Reg')
plot([allYears[idx], allYears[idx]], ylim(), 'k', lw = 2)
plot([allYears[idx] + 9, allYears[idx] + 9], ylim(), 'k', lw = 2)
plot([max(obsYears), max(obsYears)], ylim(), 'k--', lw = 2)
xlabel('Year')
ylabel('Tmax ($^\circ$C)')
xlim(min(allYears), max(allYears))
xticks(xt)
legend(bbox_to_anchor = (1.05, 1), loc = 2, borderaxespad = 0.)
grid()
# tmin
subplot(gs[4])
plot(obsYears, obs[:, 2], 'b-', label = 'Obs Tot')
plot(allYears, s.output.miTotAve, 'r-', label = 'Sim Tot Ave')
plot(allYears, s.output.miTrAve, 'k--', label = 'Sim Trend Ave')
plot(allYears, s.output.miTotReg, 'g', label = 'Sim Tot Reg')
plot(allYears, s.output.miTrReg, 'm--', label = 'Sim Trend Reg')
plot([allYears[idx], allYears[idx]], ylim(), 'k', lw = 2)
plot([allYears[idx] + 9, allYears[idx] + 9], ylim(), 'k', lw = 2)
plot([max(obsYears), max(obsYears)], ylim(), 'k--', lw = 2)
xlabel('Year')
ylabel('Tmin ($^\circ$C)')
xlim(min(allYears), max(allYears))
xticks(xt)
legend(bbox_to_anchor = (1.05, 1), loc = 2, borderaxespad = 0.)
grid()
fig.tight_layout()
show()
numPlots += 1

# three-panel plot of detrended regional pr, tmax, tmin
fig = figure(numPlots, figsize = (15, 10))
gs = gridspec.GridSpec(3, 2, width_ratios = [4, 1])
# pr
subplot(gs[0])
plot(obsYears, obsdt[:, 0], 'b-', label = 'Obs Var')
plot(allYears, s.output.prVarAve, 'r-', label = 'Sim Var Ave')
plot(allYears, s.output.prVarReg, 'g-', label = 'Sim Var Reg')
plot([allYears[idx], allYears[idx]], ylim(), 'k', lw = 2)
plot([allYears[idx] + 9, allYears[idx] + 9], ylim(), 'k', lw = 2)
plot([max(obsYears), max(obsYears)], ylim(), 'k--', lw = 2)
xlabel('Year')
ylabel('Pr (mm/d)')
xlim(min(allYears), max(allYears))
xticks(xt)
title('Detrended Regional -- Decade: [' + str(allYears[idx]) + ', ' + str(allYears[idx + 9]) + ']')
legend(bbox_to_anchor = (1.05, 1), loc = 2, borderaxespad = 0.)
grid()
# tmax
subplot(gs[2])
plot(obsYears, obsdt[:, 1], 'b-', label = 'Obs Var')
plot(allYears, s.output.maVarAve, 'r-', label = 'Sim Var Ave')
plot(allYears, s.output.maVarReg, 'g-', label = 'Sim Var Reg')
plot([allYears[idx], allYears[idx]], ylim(), 'k', lw = 2)
plot([allYears[idx] + 9, allYears[idx] + 9], ylim(), 'k', lw = 2)
plot([max(obsYears), max(obsYears)], ylim(), 'k--', lw = 2)
xlabel('Year')
ylabel('Tmax ($^\circ$C)')
xlim(min(allYears), max(allYears))
xticks(xt)
legend(bbox_to_anchor = (1.05, 1), loc = 2, borderaxespad = 0.)
grid()
# tmin
subplot(gs[4])
plot(obsYears, obsdt[:, 2], 'b-', label = 'Obs Var')
plot(allYears, s.output.miVarAve, 'r-', label = 'Sim Var Ave')
plot(allYears, s.output.miVarReg, 'g-', label = 'Sim Var Reg')
plot([allYears[idx], allYears[idx]], ylim(), 'k', lw = 2)
plot([allYears[idx] + 9, allYears[idx] + 9], ylim(), 'k', lw = 2)
plot([max(obsYears), max(obsYears)], ylim(), 'k--', lw = 2)
xlabel('Year')
ylabel('Tmin ($^\circ$C)')
xlim(min(allYears), max(allYears))
xticks(xt)
legend(bbox_to_anchor = (1.05, 1), loc = 2, borderaxespad = 0.)
grid()
fig.tight_layout()
show()
numPlots += 1

# three-panel plot of trended gridbox-level pr, tmax, tmin
for i in range(len(latsToPlot)):
    fig = figure(numPlots, figsize = (15, 10))
    gs = gridspec.GridSpec(3, 4, width_ratios = [4, 1, 4, 1])
    latIdx = latsToPlot[i]
    lonIdx = lonsToPlot[i]
    # pr
    subplot(gs[0])
    plot(obsYears, obsGriddedPr[:, latIdx, lonIdx], 'b-', label = 'Obs Tot')
    plot(allYears, s.output.prTotAr[:, latIdx, lonIdx], 'r-', label = 'Sim Tot')
    plot(allYears, s.output.prTrAr[:, latIdx, lonIdx], 'k--', label = 'Sim Trend')
    plot([allYears[idx], allYears[idx]], ylim(), 'k', lw = 2)
    plot([allYears[idx] + 9, allYears[idx] + 9], ylim(), 'k', lw = 2)
    plot([max(obsYears), max(obsYears)], ylim(), 'k--', lw = 2)
    xlabel('Year')
    ylabel('Pr (mm/d)')
    xlim(min(allYears), max(allYears))
    xticks(xt)
    title('(Lat = ' + str(lats[latIdx]) + ', Lon = ' + str(lons[lonIdx]) + ')')
    legend(bbox_to_anchor = (1.05, 1), loc = 2, borderaxespad = 0.)
    grid()
    subplot(gs[2])
    plot(obsYears, obsGriddedDtPr[:, latIdx, lonIdx], 'b-', label = 'Obs Var')
    plot(allYears, s.output.prVarAr[:, latIdx, lonIdx], 'r-', label = 'Sim Var')
    plot([allYears[idx], allYears[idx]], ylim(), 'k', lw = 2)
    plot([allYears[idx] + 9, allYears[idx] + 9], ylim(), 'k', lw = 2)
    plot([max(obsYears), max(obsYears)], ylim(), 'k--', lw = 2)
    xlabel('Year')
    ylabel('Pr (mm/d)')
    xlim(min(allYears), max(allYears))
    xticks(xt)
    title('(Lat = ' + str(lats[latIdx]) + ', Lon = ' + str(lons[lonIdx]) + ')')
    legend(bbox_to_anchor = (1.05, 1), loc = 2, borderaxespad = 0.)
    grid()
    # tmax
    subplot(gs[4])
    plot(obsYears, obsGriddedTmax[:, latIdx, lonIdx], 'b-', label = 'Obs Tot')
    plot(allYears, s.output.maTotAr[:, latIdx, lonIdx], 'r-', label = 'Sim Tot')
    plot(allYears, s.output.maTrAr[:, latIdx, lonIdx], 'k--', label = 'Sim Trend')
    plot([allYears[idx], allYears[idx]], ylim(), 'k', lw = 2)
    plot([allYears[idx] + 9, allYears[idx] + 9], ylim(), 'k', lw = 2)
    plot([max(obsYears), max(obsYears)], ylim(), 'k--', lw = 2)
    xlabel('Year')
    ylabel('Tmax ($^\circ$C)')
    xlim(min(allYears), max(allYears))
    xticks(xt)
    legend(bbox_to_anchor = (1.05, 1), loc = 2, borderaxespad = 0.)
    grid()
    subplot(gs[6])
    plot(obsYears, obsGriddedDtTmax[:, latIdx, lonIdx], 'b-', label = 'Obs Var')
    plot(allYears, s.output.maVarAr[:, latIdx, lonIdx], 'r-', label = 'Sim Var')
    plot([allYears[idx], allYears[idx]], ylim(), 'k', lw = 2)
    plot([allYears[idx] + 9, allYears[idx] + 9], ylim(), 'k', lw = 2)
    plot([max(obsYears), max(obsYears)], ylim(), 'k--', lw = 2)
    xlabel('Year')
    ylabel('Tmax ($^\circ$C)')
    xlim(min(allYears), max(allYears))
    xticks(xt)
    legend(bbox_to_anchor = (1.05, 1), loc = 2, borderaxespad = 0.)
    grid()
    # tmin
    subplot(gs[8])
    plot(obsYears, obsGriddedTmin[:, latIdx, lonIdx], 'b-', label = 'Obs Tot')
    plot(allYears, s.output.miTotAr[:, latIdx, lonIdx], 'r-', label = 'Sim Tot')
    plot(allYears, s.output.miTrAr[:, latIdx, lonIdx], 'k--', label = 'Sim Trend')
    plot([allYears[idx], allYears[idx]], ylim(), 'k', lw = 2)
    plot([allYears[idx] + 9, allYears[idx] + 9], ylim(), 'k', lw = 2)
    plot([max(obsYears), max(obsYears)], ylim(), 'k--', lw = 2)
    xlabel('Year')
    ylabel('Tmin ($^\circ$C)')
    xlim(min(allYears), max(allYears))
    xticks(xt)
    legend(bbox_to_anchor = (1.05, 1), loc = 2, borderaxespad = 0.)
    grid()
    subplot(gs[10])
    plot(obsYears, obsGriddedDtTmin[:, latIdx, lonIdx], 'b-', label = 'Obs Var')
    plot(allYears, s.output.miVarAr[:, latIdx, lonIdx], 'r-', label = 'Sim Var')
    plot([allYears[idx], allYears[idx]], ylim(), 'k', lw = 2)
    plot([allYears[idx] + 9, allYears[idx] + 9], ylim(), 'k', lw = 2)
    plot([max(obsYears), max(obsYears)], ylim(), 'k--', lw = 2)
    xlabel('Year')
    ylabel('Tmin ($^\circ$C)')
    xlim(min(allYears), max(allYears))
    xticks(xt)
    legend(bbox_to_anchor = (1.05, 1), loc = 2, borderaxespad = 0.)
    grid()    
    fig.tight_layout()
    show()
    numPlots += 1

# three-panel plot of observed pr, tmax, tmin against what they're regressed on
fig = figure(numPlots, figsize = (15, 10))
gs = gridspec.GridSpec(3, 2, width_ratios = [4, 1])
# pr
subplot(gs[0])
plot(obsYears, obs[:, 0] - obs[:, 0].mean(), 'b-', label = 'Obs')
plot(obsYears, prSmoothed, 'r-', label = 'Smoothed')
xlabel('Year')
ylabel('Pr (mm/d)')
xlim(min(obsYears), max(obsYears))
xticks(xt2)
legend(bbox_to_anchor = (1.05, 1), loc = 2, borderaxespad = 0.)
grid()
# tmax
subplot(gs[2])
plot(obsYears, obs[:, 1] - obs[:, 1].mean(), 'b-', label = 'Obs')
plot(obsYears, tmmm, 'r-', label = 'Smoothed')
xlabel('Year')
ylabel('Tmax ($^\circ$C)')
xlim(min(obsYears), max(obsYears))
xticks(xt2)
legend(bbox_to_anchor = (1.05, 1), loc = 2, borderaxespad = 0.)
grid()
# tmin
subplot(gs[4])
plot(obsYears, obs[:, 2] - obs[:, 2].mean(), 'b-', label = 'Obs')
plot(obsYears, tmmm, 'r-', label = 'Smoothed')
xlabel('Year')
ylabel('Tmin ($^\circ$C)')
xlim(min(obsYears), max(obsYears))
xticks(xt2)
legend(bbox_to_anchor = (1.05, 1), loc = 2, borderaxespad = 0.)
grid()
fig.tight_layout()
show()
numPlots += 1

# three-panel plot of regression coefficients
fig = figure(numPlots, figsize = (15, 10))
# pr
ax1 = fig.add_subplot(311)
m = createMap(ax1, lats, lons)
glons, glats = m.makegrid(len(lons), len(lats))
x, y = m(glons, glats)
lim = abs(coef0[0].flatten()).max()
cs = m.contourf(x, y, coef0[0], linspace(-lim, lim, 100))
m.colorbar(cs, location = 'right', format = '%.3f', ticks = [-lim, -lim / 2, 0, lim / 2, lim])
title('Pr')
# tmax
ax2 = fig.add_subplot(312)
m = createMap(ax2, lats, lons)
lim = abs(coef0[1].flatten()).max()
cs = m.contourf(x, y, coef0[1], linspace(-lim, lim, 100))
m.colorbar(cs, location = 'right', format = '%.3f', ticks = [-lim, -lim / 2, 0, lim / 2, lim])
title('Tmax')
# tmin
ax3 = fig.add_subplot(313)
m = createMap(ax3, lats, lons)
lim = abs(coef0[2].flatten()).max()
cs = m.contourf(x, y, coef0[2], linspace(-lim, lim, 100))
m.colorbar(cs, location = 'right', format = '%.3f', ticks = [-lim, -lim / 2, 0, lim / 2, lim])
title('Tmin')
show()
numPlots += 1

# three-panel plot of standard deviations for each variable
fig = figure(numPlots, figsize = (15, 10))
gs = gridspec.GridSpec(4, 3)
# pr
prNoiseStd = sqrt((s.output.noiseAr[:, 0] * s.output.sqrtVarDiffAr[:, 0, :]).var(axis = 0))
prNoiseStd = ma.array(prNoiseStd, mask = s.output.prTrAr.mask[0]) # apply mask
prDiffStd = obsGriddedDtPr.var(axis = 0) - prrecon.var(axis = 0)
prDiffStd[prDiffStd < 0] = 0
prDiffStd = sqrt(prDiffStd)
prObsStd = sqrt(obsGriddedDtPr.var(axis = 0))
prSimStd = sqrt(s.output.prVarAr.var(axis = 0))
prNoiseStdMasked = ma.array(resize(prNoiseStd, (1, len(lats), len(lons))), mask = prNoiseStd.mask)
prDiffStdMasked = ma.array(resize(prDiffStd, (1, len(lats), len(lons))), mask = prDiffStd.mask)
prObsStdMasked = ma.array(resize(prObsStd, (1, len(lats), len(lons))), mask = prObsStd.mask)
prSimStdMasked = ma.array(resize(prSimStd, (1, len(lats), len(lons))), mask = prSimStd.mask)
prNoiseStdAve = areaAverage(prNoiseStdMasked, lats)[0]
prDiffStdAve = areaAverage(prDiffStdMasked, lats)[0]
prObsStdAve = areaAverage(prObsStdMasked, lats)[0]
prSimStdAve = areaAverage(prSimStdMasked, lats)[0]
lim = max(prNoiseStd.max(), prDiffStd.max())
lim2 = max(prObsStd.max(), prSimStd.max())
# difference
m = createMap(subplot(gs[0]), lats, lons)
cs = m.contourf(x, y, prDiffStd, linspace(0, lim, 100))
m.colorbar(cs, location = 'right', format = '%.2f', ticks = [0, lim / 2, lim])
title('Pr Reconstructed - Observed\nAA = {:f}'.format(prDiffStdAve))
# noise 
m = createMap(subplot(gs[3]), lats, lons)
cs = m.contourf(x, y, prNoiseStd, linspace(0, lim, 100))
m.colorbar(cs, location = 'right', format = '%.2f', ticks = [0, lim / 2, lim])
title('Pr Noise Added\nAA = {:f}'.format(prNoiseStdAve))
# observation 
m = createMap(subplot(gs[6]), lats, lons)
cs = m.contourf(x, y, prObsStd, linspace(0, lim2, 100))
m.colorbar(cs, location = 'right', format = '%.2f', ticks = [0, lim2 / 2, lim2])
title('Pr Obs\nAA = {:f}'.format(prObsStdAve))
# simulation
m = createMap(subplot(gs[9]), lats, lons)
cs = m.contourf(x, y, prSimStd, linspace(0, lim2, 100))
m.colorbar(cs, location = 'right', format = '%.2f', ticks = [0, lim2 / 2, lim2])
title('Pr Sim\nAA = {:f}'.format(prSimStdAve))
# tmax
maNoiseStd = sqrt((s.output.noiseAr[:, 1] * s.output.sqrtVarDiffAr[:, 1, :]).var(axis = 0))
maNoiseStd = ma.array(maNoiseStd, mask = s.output.maTrAr.mask[0]) # apply mask
maDiffStd = obsGriddedDtTmax.var(axis = 0) - marecon.var(axis = 0)
maDiffStd[maDiffStd < 0] = 0
maDiffStd = sqrt(maDiffStd)
maObsStd = sqrt(obsGriddedDtTmax.var(axis = 0))
maSimStd = sqrt(s.output.maVarAr.var(axis = 0))
maNoiseStdMasked = ma.array(resize(maNoiseStd, (1, len(lats), len(lons))), mask = maNoiseStd.mask)
maDiffStdMasked = ma.array(resize(maDiffStd, (1, len(lats), len(lons))), mask = maDiffStd.mask)
maObsStdMasked = ma.array(resize(maObsStd, (1, len(lats), len(lons))), mask = maObsStd.mask)
maSimStdMasked = ma.array(resize(maSimStd, (1, len(lats), len(lons))), mask = maSimStd.mask)
maNoiseStdAve = areaAverage(maNoiseStdMasked, lats)[0]
maDiffStdAve = areaAverage(maDiffStdMasked, lats)[0]
maObsStdAve = areaAverage(maObsStdMasked, lats)[0]
maSimStdAve = areaAverage(maSimStdMasked, lats)[0]
lim = max(maNoiseStd.max(), maDiffStd.max())
lim2 = max(maObsStd.max(), maSimStd.max())
# difference
m = createMap(subplot(gs[1]), lats, lons)
cs = m.contourf(x, y, maDiffStd, linspace(0, lim, 100))
m.colorbar(cs, location = 'right', format = '%.2f', ticks = [0, lim / 2, lim])
title('Tmax Reconstructed - Observed\nAA = {:f}'.format(maDiffStdAve))
# noise
m = createMap(subplot(gs[4]), lats, lons)
cs = m.contourf(x, y, maNoiseStd, linspace(0, lim, 100))
m.colorbar(cs, location = 'right', format = '%.2f', ticks = [0, lim / 2, lim])
title('Tmax Noise Added\nAA = {:f}'.format(maNoiseStdAve))
# observation 
m = createMap(subplot(gs[7]), lats, lons)
cs = m.contourf(x, y, maObsStd, linspace(0, lim2, 100))
m.colorbar(cs, location = 'right', format = '%.2f', ticks = [0, lim2 / 2, lim2])
title('Tmax Obs\nAA = {:f}'.format(maObsStdAve))
# simulation
m = createMap(subplot(gs[10]), lats, lons)
cs = m.contourf(x, y, maSimStd, linspace(0, lim2, 100))
m.colorbar(cs, location = 'right', format = '%.2f', ticks = [0, lim2 / 2, lim2])
title('Tmax Sim\nAA = {:f}'.format(maSimStdAve))
# tmin
miNoiseStd = sqrt((s.output.noiseAr[:, 2] * s.output.sqrtVarDiffAr[:, 2, :]).var(axis = 0))
miNoiseStd = ma.array(miNoiseStd, mask = s.output.miTrAr.mask[0]) # apply mask
miDiffStd = obsGriddedDtTmin.var(axis = 0) - mirecon.var(axis = 0)
miDiffStd[miDiffStd < 0] = 0
miDiffStd = sqrt(miDiffStd)
miObsStd = sqrt(obsGriddedDtTmin.var(axis = 0))
miSimStd = sqrt(s.output.miVarAr.var(axis = 0))
miNoiseStdMasked = ma.array(resize(miNoiseStd, (1, len(lats), len(lons))), mask = miNoiseStd.mask)
miDiffStdMasked = ma.array(resize(miDiffStd, (1, len(lats), len(lons))), mask = miDiffStd.mask)
miObsStdMasked = ma.array(resize(miObsStd, (1, len(lats), len(lons))), mask = miObsStd.mask)
miSimStdMasked = ma.array(resize(miSimStd, (1, len(lats), len(lons))), mask = miSimStd.mask)
miNoiseStdAve = areaAverage(miNoiseStdMasked, lats)[0]
miDiffStdAve = areaAverage(miDiffStdMasked, lats)[0]
miObsStdAve = areaAverage(miObsStdMasked, lats)[0]
miSimStdAve = areaAverage(miSimStdMasked, lats)[0]
lim = max(miNoiseStd.max(), miDiffStd.max())
lim2 = max(miObsStd.max(), miSimStd.max())
# difference
m = createMap(subplot(gs[2]), lats, lons)
cs = m.contourf(x, y, miDiffStd, linspace(0, lim, 100))
m.colorbar(cs, location = 'right', format = '%.2f', ticks = [0, lim / 2, lim])
title('Tmin Reconstructed - Observed\nAA = {:f}'.format(miDiffStdAve))
# noise
m = createMap(subplot(gs[5]), lats, lons)
cs = m.contourf(x, y, miNoiseStd, linspace(0, lim, 100))
m.colorbar(cs, location = 'right', format = '%.2f', ticks = [0, lim / 2, lim])
title('Tmin Noise Added\nAA = {:f}'.format(miNoiseStdAve))
# observation 
m = createMap(subplot(gs[8]), lats, lons)
cs = m.contourf(x, y, miObsStd, linspace(0, lim2, 100))
m.colorbar(cs, location = 'right', format = '%.2f', ticks = [0, lim2 / 2, lim2])
title('Tmin Obs\nAA = {:f}'.format(miObsStdAve))
# simulation
m = createMap(subplot(gs[11]), lats, lons)
cs = m.contourf(x, y, miSimStd, linspace(0, lim2, 100))
m.colorbar(cs, location = 'right', format = '%.2f', ticks = [0, lim2 / 2, lim2])
title('Tmin Sim\nAA = {:f}'.format(miSimStdAve))
show()
numPlots += 1

# compute correlations on detrended data
print 'Detrended observation correlation = \n', corrcoef(obsdt.T)

simOutAve = array(zip(s.output.prVarAve, s.output.maVarAve, s.output.miVarAve))
print 'Detrended simulation correlation (ave) = \n', corrcoef(simOutAve.T)

simOutReg = array(zip(s.output.prVarReg, s.output.maVarReg, s.output.miVarReg))
print 'Detrended simulation correlation (reg) = \n', corrcoef(simOutReg.T)

# compute variances on detrended data
obsVar = obsdt.var(axis = 0)
simPrVarAveAll = s.output.prVarAve.var()
simPrVarAveObs = s.output.prVarAve[: len(obsYears)].var()
simPrVarRegAll = s.output.prVarReg.var()
simPrVarRegObs = s.output.prVarReg[: len(obsYears)].var()
simTmaxVarAveAll = s.output.maVarAve.var()
simTmaxVarAveObs = s.output.maVarAve[: len(obsYears)].var()
simTmaxVarRegAll = s.output.maVarReg.var()
simTmaxVarRegObs = s.output.maVarReg[: len(obsYears)].var()
simTminVarAveAll = s.output.miVarAve.var()
simTminVarAveObs = s.output.miVarAve[: len(obsYears)].var()
simTminVarRegAll = s.output.miVarReg.var()
simTminVarRegObs = s.output.miVarReg[: len(obsYears)].var()

print '==================='
print 'Detrended observed pr variance =', obsVar[0]
print 'Simulated pr variance (ave) =', simPrVarAveAll
print 'Simulated pr variance (reg) =', simPrVarRegAll
print 'Simulated pr variance in observational period (ave) =', simPrVarAveObs
print 'Simulated pr variance in observational period (reg) =', simPrVarRegObs
print '==================='
print 'Detrended observed tmax variance =', obsVar[1]
print 'Simulated tmax variance (ave) =', simTmaxVarAveAll
print 'Simulated tmax variance (reg) =', simTmaxVarRegAll
print 'Simulated tmax variance in observational period (ave) =', simTmaxVarAveObs
print 'Simulated tmax variance in observational period (reg) =', simTmaxVarRegObs
print '==================='
print 'Detrended observed tmin variance =', obsVar[2]
print 'Simulated tmin variance (ave) =', simTminVarAveAll
print 'Simulated tmin variance (reg) =', simTminVarRegAll
print 'Simulated tmin variance in observational period (ave) =', simTminVarAveObs
print 'Simulated tmin variance in observational period (reg) =', simTminVarRegObs

# compute serial autocorrelations