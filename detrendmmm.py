"""

Detrend signals by regression on the multimodel mean temperature signal.

Usage: xdtar,lines,fmar = detrendmmm.dt(sig,yr0,sres), where

sig is the (vector) signal,
yr0 is the first data year and
sres is either 'a1b', 'a2' or 'rcp45'

xdtar is the array of detrended signals,
lines is the array of fitted lines
fmar is the dictionary of fit diagnostics

"""

import pkl0 as pkl, pickle
from numpy import zeros
from scipy.stats import linregress as lm

def dt(sig,yr0,sres):
    if sres == 'a1b':
        x0 = pkl.l('tMMMsm_a1b_1901-2055')
    elif sres == 'a2':
        x0 = pkl.l('tMMMsm_a2_1901-2055')
    elif sres == 'rcp45':
        x0 = pkl.l('cmip5/tasav_cmip5_comb_sm_1901-2095')

    x0 -= x0.mean()                          # Avoid const terms O(300)

    yrs0 = range(1901,2056)
    start = yrs0.index(yr0)
    ss = sig.shape
    yrs = yrs0[start:start+ss[0]]
    x = x0[start:start+ss[0]]

    outar = zeros(ss)
    if len(ss) == 1:
        y = sig
        fmdic = lm(x,y)
        lines = fmdic[0]*x + fmdic[1]
        outar = y - lines
    else:
        lines = zeros(sig.shape)
        fmdic = {}
        for i in range(ss[1]):
            y = sig[:,i]
            fm = lm(x,y)
            line = fm[0]*x + fm[1]
            lines[:,i] = line
            outar[:,i] = y - line
            fmdic[i] = fm

    return outar, lines, fmdic

#################
    
