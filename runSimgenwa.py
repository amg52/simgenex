# custom
from sim import Sim
# builtin
from pylab import savefig
import matplotlib.pyplot as plt
from numpy import min, max, log, exp, linspace

makePlots = True
saveFigs = True
latidx = 0
lonidx = 0

# ==========
# RUN SIMGEN
# ==========
p = {'trendMethod': 'Log', 'perPrChange': 0.2} # other options provided by default
s = Sim(p)
s.run()

if makePlots:
    # close all prior plots
    plt.close('all')
    
    # extract variables
    lats, lons = s.input.loadLatLonGrid()
    obsTmmm, simTmmm, allTmmm = s.input.loadMultimodelTemp()
    raw = s.input.loadYearlyObservations()
    obslen = s.input.obslen()
    simlen = s.input.simlen()
    prforcedar = s.output.prTrAr
    obsSeasPr = s.input.loadGriddedObservations()[0]
    obsstartyear = s.input.obsstartyear()
    impChange = s.input.perPrChange()
    prforced = s.output.prTrReg
    prarea = s.output.prTrAve
    prav = prforcedar.sum(axis = 2).sum(axis = 1) / (~prforcedar.mask[0]).sum()
    time = range(obsstartyear, obsstartyear + obslen + simlen)
    
    # calculate what simulated precipitation trend should be according to impChange
    offset1 = log(prforcedar[obslen + 6, latidx, lonidx]) - impChange * simTmmm[6]
    primp = exp(impChange * simTmmm[6 :] + offset1)
    
    fig, (ax1, ax2) = plt.subplots(2, 1)
    # plot example gridbox trend against temperature
    ax1.plot(allTmmm, prforcedar[:, latidx, lonidx], label = 'Gridbox trend')
    ax1.plot(obsTmmm, obsSeasPr[:, latidx, lonidx], 'gx', label = 'Observed')
    ax1.plot(allTmmm[obslen + 6 :], primp, 'r', label = 'Forced % pr')
    ax1.set_xlabel('Centered global mean temperature')
    ax1.set_ylabel('Precipitation (mm/d)')
    ax1.set_title('(lat, lon) = ({0:.2f}, {1:.2f})'.format(lats[latidx], lons[lonidx]))
    ax1.set_xlim(min(allTmmm), max(allTmmm))
    ax1.set_xticks(linspace(min(allTmmm), max(allTmmm), 5))
    ax1.legend()
    # plot example gridbox trend against time
    ax2.plot(time, prforcedar[:, latidx, lonidx], label = 'Gridbox trend')
    ax2.plot(time[: obslen], obsSeasPr[:, latidx, lonidx], 'go-', label = 'Observed')
    ax2.set_xlabel('Year')
    ax2.set_ylabel('Precipitation (mm/d)')
    ax2.set_xlim(time[0], time[-1])
    ax2.set_xticks(range(time[0], time[-1], 20))
    ax2.legend()
    plt.show()
    if saveFigs: savefig('simFigLog1.png')
    
    fig = plt.figure()
    # plot residuals
    y = obsSeasPr[:, latidx, lonidx]
    yhat = prforcedar[: obslen, latidx, lonidx]
    plt.plot(yhat, y - yhat, 'bx')
    plt.xlabel('Estimated precipitation (mm/d)')
    plt.ylabel('Residuals (mm/d)')
    plt.title('(lat, lon) = ({0:.2f}, {1:.2f})'.format(lats[latidx], lons[lonidx]))
    plt.show()
    if saveFigs: savefig('simFigLog2.png')

    fig, (ax3, ax4) = plt.subplots(2, 1)
    # plot precipitation trend against temperature
    ax3.plot(allTmmm, prarea, label = 'Area averaged trend')
    ax3.plot(allTmmm, prav, 'k', label = 'Averaged trend')
    ax3.plot(allTmmm, prforced, 'r', label = 'Regional trend')
    ax3.plot(allTmmm[0 : obslen], raw[:, 0], 'gx', label = 'Observed')
    ax3.set_xlabel('Centered global mean temperature')
    ax3.set_ylabel('Precipitation (mm/d)')
    ax3.set_xlim(min(allTmmm), max(allTmmm))
    ax3.set_xticks(linspace(min(allTmmm), max(allTmmm), 5))
    ax3.legend(loc = 2)
    # plot precipitation trend against time
    ax4.plot(time, prarea, label = 'Area averaged trend')
    ax4.plot(time, prav, 'k', label = 'Averaged trend')
    ax4.plot(time, prforced, 'r', label = 'Regional trend')
    ax4.plot(time[0 : obslen], raw[:, 0], 'go-', label = 'Observed')
    ax4.set_xlabel('Year')
    ax4.set_ylabel('Precipitation (mm/d)')
    ax4.set_xlim(time[0], time[-1])
    ax4.set_xticks(range(time[0], time[-1], 20))
    ax4.legend(loc = 2)
    plt.show()
    if saveFigs: savefig('simFigLog3.png')