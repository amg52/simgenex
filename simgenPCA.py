"""

$ Revised 2012-11-24, Arthur M. Greene $

New in version PCA: individual grids are detrended, then
standardized. The three fields are then concatentated to produce a
supermatrix on which PCA is performed. A VAR model (possibly later
converted to a reduced state-space model) is fit to PC time series,
then used to generate the simulation sequences at all gridboxes at
once. The code below ingests full 3-D fields, rather than cycling
through gridboxes. The PCA is performed in the offline module
makebigarray.py, the reconstruction step in recon.py.

New in version SESA:

Regional precip trend selected by parameter 'bfactor' (balance
factor), which ranges from 0 to 1. The value denotes the fractional
balance between ozone and global temperature covariates. A value of
0.5 will produce a small positive pr trend, making an 'elbow' with the
obs 20c record.

----------------

New in version 9:
    
1. The multimodel mean global mean temperature signal, as well as the
regional precipitation respons(es) are derived from the CMIP5 model
ensemble. In previous iterations the CMIP3 ensemble was utilized.

2. The 21C precipitation trend is selected by quantile, from a normal
distribution fitted to the empirical distribution of trends in the
CMIP5 ensemble. This is done in terms of the model percent change in
annual mean precipication (for the region 30-35S, 17-23E) per degree
of (individual) model global warming.

3. Block resampling is now performed using a k-nearest-neighbor
scheme, conditional on either regional mean precipitation or on the
full three-component (pr, Tmax, Tmin) variable vector, if the M = 1
option is chosen.  In the latter case the Mahalanobis distance metric
is used, with weights assigned to pr, Tmax and Tmin. This is
implemented in function 'rkm' near the end of the module code.

----------------

Generate stochastic simulations with which to drive ACRU. This script
processes a single station, but many stations can be co-processed,
using the same 'regsim' input and value for 'ix', to produce a
matched, spatially coherent ensemble.

Usage: gen(simix,seas,bfactor,obsstartyear,simstartyear,dirname,write=0,\
        nvar=3, simlen=41,locate=2031, xval=1, UY=0):

Inputs:

simix is the starting index into this file for the simulation. This
index must be determined, perhaps by an examination of decadal
excursions and their relative likelihood, prior to running simgen.
simix is the starting index into this file for the simulation. This
index must be determined, perhaps by an examination of decadal
excursions and their relative likelihood, prior to running simgen.

[NEW] trendq is the trend quantile, respecting that the 21C trend is
now drawn from a distribution. This distribution has been precomputed
using data from a number of the CMIP5 simulations, and is hard-coded
into simgen. It may be updated as more CMIP5 models come online (14
models are used in the current iteration, of 2011-11-22.

[DEPRECATED, no longer accessible] alpha, which still exists in the
code, is a mixing coefficient. For a given catchment, the future
precipitation trend is now a weighted average of the imposed
(CMIP5-based) regional trend and a local trend derived from the 20C
annual mean precip observations, as regressed on global mean
temperature. The weights are 1/s for the regional trend and alpha/s
for the local signal, where s = 1 + alpha. Thus, alpha = 0 results in
the regional trend being applied uniformly across stations, while
alpha = 1 produces an equally-weighted average of regional and local
trends. Nonzero alpha values have the effect of causing the individual
catchment trends to scatter around the regional mean value.

dirname is the subdirectory of the directory named 'ACRU' to which to
write output, and

fname is the name of the (long) simulation file, presently 500 kyr in
length,

simlen (lately 66, for the period 2000-2065) is the desired simulation
length, in years.

locate indicates the simulation year at which to place the sequence
beginning at index simix,

xval is a flag indicating whether to generate simulations for the
1950-2000 period for the purposes of cross validation. If xval = 1,
simlen will be reset to 50, regardless of the value input.

M is a flag for using the Mahalanobis distance measure, for the
resampling scheme.

Outputs:

datmat is the daily output (i.e., simulation) matrix,

fmat is the matrix of annualized forced trends and

scalemat contains the annualized simulation values exclusive of
trends. To plot the annualized simulation, including forced trends,
use the sum of fmat and scalemat.

When xval = 0, the actual pr,tmax,tmin is used for the 1950-1999
period . (The simulated segment is then grafted onto the end.) This
could be changed, but this is a less important option now that the
xval flag is available.

Proceedural map:

obshis file is 50 yr of daily data, simulation is (66) yr of future
data, concatenated to the former. How is the simulation to be
constructed? It has three components:

1. Forced: For pr, from IPCC RCP4.5; for Tmax, Tmin from 20c
sensitivity.  2. Interannual-to-decadal: Intrinsic to simulation.
3. Daily/seasonal: Resampled from observations.

How accomplished:

1. Sim may have a small but nonzero mean and possibly also nonzero
trend. Because these arise only from sampling variations they are
removed, by linear detrending.

2. Sensitivities to anthropogenic forcing: Tmax, Tmin are regressed,
station by station, on the global tmmm signal. These sensitivites are
carried over to produce 21c trends, which vary by station. For pr the
sensitivity is computed in terms of MMM WCA (30-35S, 17-25E) pr
regressed on the MMM 21c global mean temperature, which yields a
strongly negative trend (-23% for JJA by 2080-2100). The resultant
trends are injected at all stations (but will be modified by the
random nature of added noise).

3. Compute local sensitivity to 'regional decadal' signal by
regressing each obshis record (detrended in the same way as was
regional) onto the detrended regional record. At present, the regional
record is the 171-station mean.

4. Compute the fitted values from above. Scale simulated series to
match. Add uncorrelated noise to bring variance of the latter and that
of the station record into agreement. Each variable is processed
separately.

5. Inject the scaled values (scalemat) into the station record,
removing in the process the latter's intrinsic annual mean values and
add the trend (i.e., forced) component at the same time. Choose at
random (i.e., resample) the observed year from which the subannual
variations derive. Since this version produces monthly, as opposed to
daily, output, the distinction been leap and nonleap years can be
ignored.

6. Write out (if write == 1).

"""

import cPickle as pickle, detrend2, detrendmmm, numpy as np
from netCDF4 import Dataset as nc
import aav, ncwrite, recon, sys
from numpy.random import rand, standard_normal as sn
from numpy.ma import masked_greater, masked_where
from scipy.stats import linregress as lm

def gen(simix,seas,bfactor,obsstartyear,simstartyear,dirname,write=0,\
        nvar=3, simlen=43, locate=2031, xval=1, UY=0):
    
    """ Setup...

    Some new parameters:
    
    obsstart: what year to take as first year of obs (not necessarily
    the first year in the record)

    simstart: First year of simulation (2000 for S. Afr, 2010 for
    SESA, etc...) The obs here runs from 1901-2009, so the nominal
    simstart would be 2010.

    locate: At what calendar year should the simulated anomaly begin?

    """

#########################################################
#
# First chore: Assemble and condition the necessary files
#
#########################################################

    if obsstartyear < 1902:
        print 'obsstartyear must be 1902 or later!'
        return
    obsstartyear -= 1                        # Restored at end of code
    simlen += 1                              # Ditto
    
    if nvar == 2:
        print 'Not ready for 2-variable case just yet... bye!'
        return
    elif nvar == 3:
        simfname = 'sim_dse/sim5SESA_dt3_stdz_areawt_'+seas+'_100kyr.dat'
    sys.stdout.flush()
    regsim0 = np.loadtxt(simfname)           # 100000 yr x 5 pcs, e.g.
#    print 'Raw simulation file (PC time series) is read...'

# If nvar==3 recommend obsstartyear = 1961, to avoid too much filled data
    obslen = simstartyear - obsstartyear

# Here, we slice the long simulation sequence
    """ Recall that these are PCs rather than regional means... """
    if xval == 0:
        offset   = simstartyear - locate     # < 0
        simstart = simix + offset            # Ignore 20C in this case
        simend   = simstart + simlen
    elif xval == 1:
        offset   = simstartyear - locate - obslen # Include 20C
        simstart = simix + offset
        simend   = simstart + obslen + simlen
    
    regsim = regsim0[simstart:simend,:]
    print 'Shape of trimmed, augmented simulation file:', regsim.shape
    sys.stdout.flush()

# Now fetch the multimodel mean, global mean temperature...
    tmmm = pickle.load(open('pickled/cmip5/tasav_cmip5_comb_sm_1901-2095.p'))
    
# Next, the univariate observational records, derived from CRU TS3.1,
# GPCC, etc.  These are the DETRENDED series, used for calibrating the
# univariate seasonal sims.  Note: pr in these sims may be from TS3.1,
# if so should be updated. (The new filename, starting with
# "datSESAavdt..." is based on TS3.10.01).

    if nvar == 2:
        regobsfname = 'dat/dat'+seas+'_25-40S_45-65W_mmda_dt.dat'
    elif nvar == 3:
        regobsfname  = 'dat/datSESAavdt_'+seas+'_25-40S_45-65W.dat'

    begix = obsstartyear - 1901              # Starts with SONDJF 1901-02
    endix = 108                              # Full length is 108: 1901-2008
    regobs = np.loadtxt(regobsfname)         # Regional mean series

    obspr = regobs[begix:endix,0]            # Regional obs, trimmed
    if nvar == 2:
        obsmn = regobs[begix:endix,1]
    elif nvar == 3:
        obsma = regobs[begix:endix,1]
        obsmi = regobs[begix:endix,2]

    print 'For regsim: offset, simstart, simend, len:',\
          offset, simstart, simend, simend-simstart
    print 'For regobs: begix, endix, len:',\
          begix, endix, endix-begix

# Now the gridded observational seasonal records, for calibrating the
# seasonal gridbox-level simulations and trends...

    fnamegrid = 'pickled/pr-ma-mi_SESA_SONDJF_mmda.p'
    prseas,maseas,miseas = pickle.load(open(fnamegrid))
    sgrid = prseas.shape                     # Like (108,30,40)
    nlats = sgrid[1]
    nlons = sgrid[2]
    print 'prseas.mean and shape:', prseas.mean(), sgrid
    lats,lons = pickle.load(open('pickled/latlon_SESA.p'))

    print 'Shape of (all/both) seasonal variables:', prseas.shape
    sseas = prseas.shape

# The values below derive from offline computations, done individually
# with Tmmm and O3. The two regression fits are blended according to
# bfactor. If bfactor == 0, ozone is controlling, and the future
# regional trend will be < 0; if bfactor == 1, Tmmm is controlling and
# the future trend will be > 0. (For bfactor = 0.5 the trend is > 0,
# but smaller.)

    trendtmm = np.loadtxt('dat/prhat_tmmm_mmda_'+seas+'.dat')
    trendO3  = np.loadtxt('dat/prhat_O3_mmda_'+seas+'.dat')
    prforced = bfactor*trendtmm + (1-bfactor)*trendO3  

# Prepare global mean Tmmm signal, for projecting tmp,ma,mi...
    tmmm -= tmmm.mean()
    xmm  = tmmm[begix:endix+simlen]          # Multimodel for 1901-(2065)
    x20c = tmmm[begix:endix]                  # 1902-2009, e.g.
    x21c = tmmm[endix:endix+simlen]           # (2010) onward

    print 'lens for x20c, x21c:', len(x20c), len(x21c)

# Fetch the 'allmos' data and the climatologies. The latter are
# dictionaries with keys 'Jan', 'Feb'... and values consisting of 2-D
# climatological pr (or tmp) for the respective month. These will be
# used for downscaling to monthly, but need be fetched only once...

    fprallmos =  nc('cdf_nc/CRU/prTS3.10.01_25-40S_45-65W_allmos.nc')
    prallmos = fprallmos.variables['pr'][:]/30.  # -> mm/d
    prsimallmos = prallmos[12*begix:12*endix]
    if nvar == 2:
        fmnallmos = nc('cdf_nc/CRU/tmpTS3.1_25-40S_45-65W_allmos.cdf')
        mnallmos = fmnallmos.variables['tmp'][:]                
        mnsimallmos = mnallmos[12*begix:12*endix]
    elif nvar == 3:
        fmaallmos = nc('cdf_nc/CRU/tmxTS3.1_25-40S_45-65W_allmos.cdf')
        fmiallmos = nc('cdf_nc/CRU/tmnTS3.1_25-40S_45-65W_allmos.cdf')
        maallmos = fmaallmos.variables['tmx'][:]                
        miallmos = fmiallmos.variables['tmn'][:]
        masimallmos = maallmos[12*begix:12*endix]
        misimallmos = miallmos[12*begix:12*endix]

    taxallmos = fprallmos.variables['time'][:]
    taxsimallmos = taxallmos[12*begix:12*endix]
    tunitsallmos = fprallmos.variables['time'].units
    lats = fprallmos.variables['lat'][:]
    lons = fprallmos.variables['lon'][:]
    fprallmos.close()
    fmaallmos.close()
    fmiallmos.close()
    
# Note: May wish to switch climpost3 to climpost2 below for
# consistency w/eofs, etc. A checkable suggestion...
    mos = pickle.load(open('pickled/months.p'))
    climpost2 = 'TS3.1or10_25-40S_45-65W_climdic.p'
    """ TS3.10 for pr, 3.1 for tmp,tmx,tmn """
    climpost3 = 'TS3.1or10_25-40S_45-65W_1961-2009_climdic.p'    
    if nvar == 2:
        prclimdic = pickle.load(open('pickled/pr'+climpost2))
        mnclimdic = pickle.load(open('pickled/tmp'+climpost2))
    elif nvar == 3:
        """ tmax, min have many filled values prior to 1961 """
        prclimdic = pickle.load(open('pickled/pr'+climpost2))        
        maclimdic = pickle.load(open('pickled/tmx'+climpost2))
        miclimdic = pickle.load(open('pickled/tmn'+climpost2))

    newlen   = obslen+simlen          # Like 107 + 43 = 150
    newmolen = 12 * (obslen + simlen) # 12 months per year
    prsim = 1e20*np.ones((newmolen,nlats,sgrid[2]),'f')  # 3-D, monthly res
    if nvar == 2:
        mnsim = 1e20*np.ones((newmolen,nlats,sgrid[2]),'f')
    elif nvar == 3:
        masim = 1e20*np.ones((newmolen,nlats,sgrid[2]),'f')
        misim = 1e20*np.ones((newmolen,nlats,sgrid[2]),'f')

# Assemble and inject climatology for the non-seasonal months First,
# construct a n-month 'chunk' with the apprpriate climatological
# values; then, inject it into prsim at the appropriate
# indices.

    if seas == 'DJF':                        # We'll do this later...
        climmos = ['Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov']
        """ etc. Fill this in, or generalize, later...."""
    elif seas == 'SONDJF':
        climmos = ['Mar','Apr','May','Jun','Jul','Aug']
        climchunkpr = 1e20*np.ones((6,nlats,sgrid[2]))
        if nvar == 2:
            climchunkmn = 1e20*np.ones((6,nlats,sgrid[2]))
            ct = 0
            for i in climmos:
                climchunkpr[ct] = prclimdic[i]
                climchunkmn[ct] = mnclimdic[i]
                ct += 1
        elif nvar == 3:
            climchunkma = 1e20*np.ones((6,nlats,sgrid[2]))
            climchunkmi = 1e20*np.ones((6,nlats,sgrid[2]))
            ct = 0
            for i in climmos:
                climchunkpr[ct] = prclimdic[i]    # 2-D fields
                climchunkma[ct] = maclimdic[i]
                climchunkmi[ct] = miclimdic[i]
                ct += 1

        if xval == 0:                        # Still seasonal...
            prsim[:12*obslen,:,:] = prsimallmos
            if nvar == 2:
                mnsim[:12*obslen] = mnsimallmos
            elif nvar == 3:
                masim[:12*obslen] = masimallmos
                misim[:12*obslen] = misimallmos

            for i in range(obslen,obslen+simlen):
                climix = 12*i + 2
                prsim[climix:climix+6] = climchunkpr
                if nvar == 2:
                    mnsim[climix:climix+6] = climchunkmn
                elif nvar == 3:
                    masim[climix:climix+6] = climchunkma
                    misim[climix:climix+6] = climchunkmi

        elif xval == 1:
            for i in range(obslen+simlen):
                climix = 12*i + 2
                prsim[climix:climix+6] = climchunkpr
                if nvar == 2:
                    mnsim[climix:climix+6] = climchunkmn
                elif nvar == 3:
                    masim[climix:climix+6] = climchunkma
                    misim[climix:climix+6] = climchunkmi

# This completes preliminary file preparation.

#####################################################################
#    
# Now the k-NN implementation: First, generate the sequence of years
# that will be resampled, using the eofs, pcs and recon.py. The
# "feature vector" here is seasonal and has either two or three (i.e.,
# nvar) components.  It is compared with the observed (non-detrended)
# record, the k nearest neighbors selected and the resampled year
# chosen from among them with probabilities proportional to 1/n, n =
# 1,2,...k.
#
# Note: It might be worth trying to rewrite this section so that the
# feature vector extends over all the spatial domain. Then (a)
# matching would be far more precise (or noisy) and (b) it would make
# more sense to have prcorrect, macorrect, etc., be spatially
# disaggregated. See version simgenPCA1 for implementation.
#
#####################################################################

# First, fetch the *non-detrended* data...
    if nvar == 2:
        obs_raw   = np.loadtxt('dat/dat'+seas+'_25-40S_45-65W_mmda.dat')
        obsmn_raw = obs_raw[begix:endix,1]   # To be regressed on Tmmm
    elif nvar == 3:
        fnameraw = 'dat/datSESAav_SONDJF_25-40S_45-65W.dat'
        obs_raw0 = np.loadtxt(fnameraw)
        obs_raw = obs_raw0[begix:endix,:]
        obspr_raw = obs_raw[:,0]
        obsma_raw = obs_raw[:,1]
        obsmi_raw = obs_raw[:,2]

        print 'lens:', len(tmmm), len(x20c), len(x21c), len(obspr_raw)

    C = np.matrix(np.cov(obs_raw.T))
    CI = C.I                                 # Inverse covariance matrix...

# Next, compute the regional mean future pr, mn (or ma, mi) signal
# (20C as well as future, iff xval==1), so the feature vectors can be
# computed.

    pr20forced = prforced[begix:endix]       # Regional mean...
    pr21forced = prforced[endix:endix+simlen]

    if nvar == 2:
        fmmn = lm(x20c,obsmn_raw)            # Should be same length...
        mn21forced = fmmn[1] + fmmn[0]*x21c
    elif nvar == 3:
        ma21forced = 1.0E+20 * np.ones(simlen)   # Need for M = 1
        mi21forced = 1.0E+20 * np.ones(simlen)
        print 'len(x20c), len(obsma_raw):', len(x20c), len(obsma_raw)
        fmma = lm(x20c,obsma_raw)                # Trend fit to 20C
        fmmi = lm(x20c,obsmi_raw)
        ma21forced = fmma[1] + fmma[0]*x21c  # Project into 21C
        mi21forced = fmmi[1] + fmmi[0]*x21c

# These regional means are constructed for use by k-NN
    sim21total = 1.0E+20 * np.ones((simlen,nvar))

# Compute 21C forced + 21C sim for all variables
    if xval == 0:                            # No k-NN for 20C in this case
        pr21total = pr21forced + simpr
        if nvar == 2:
            mn21total = mn21forced + simmn
        elif nvar == 3:
            ma21total = ma21forced + simma
            mi21total = mi21forced + simmi
        
    elif xval == 1:                          # Simulate 20C also
        eo0,eo1,eo2,lam,vf,pc,meanar,stdar,coef0,coef1 = pickle.load(\
            open('pickled/eo0.eo1.eo2.lam.vf.pc.meanar.stdar.coef0.coef1.p'))
        prrecon = recon.r1(eo0,regsim,'pr', 5,meanar,stdar,lats)  # mm/d
        marecon = recon.r1(eo1,regsim,'tmx',5,meanar,stdar,lats)
        mirecon = recon.r1(eo2,regsim,'tmn',5,meanar,stdar,lats)

# No discontinuity in prrecon, marecon, mirecon...
    
        simpr0 = aav.av(prrecon,lats)        # pr aav, no trend
        simma0 = aav.av(marecon,lats)        # ma...
        simmi0 = aav.av(mirecon,lats)        # mi

# Correct the variances...

        sim0 = 1e20*np.ones((len(simpr0),3))
        sim0[:,0] = simpr0
        sim0[:,1] = simma0
        sim0[:,2] = simmi0

        obsvar = regobs.var(axis=0)
        simvar = sim0.var(axis=0)
        print 'obsvar, simvar:', obsvar, simvar
        vardiff = obsvar - simvar
        for i in range(len(vardiff)):
            if vardiff[i] < 0:
                print 'Variance anomaly at component', i, vardiff[i]
                vardiff[i] = 0
        stddiff = np.sqrt(vardiff)
        simcorrect = sim0 + sn(sim0.shape) * stddiff
        print 'obsvar, simvar:', obsvar, simcorrect.var(axis=0)
        vardiff1 = obsvar - simcorrect.var(axis=0)
        print 'vardiff, vardiff1:', vardiff, vardiff1

        sim20total = 1.0E+20*np.ones((obslen,nvar))    # Not needed for xval=0
        print 'sim20total.shape:', sim20total.shape
        pr20total = pr20forced + simcorrect[:obslen,0]
        pr21total = pr21forced + simcorrect[obslen:obslen+simlen,0]
        if nvar == 2:
            mn21total = mn21forced + simmn[obslen:obslen+simlen]
        elif nvar == 3:
            ma21total = ma21forced + simcorrect[obslen:obslen+simlen,1]
            mi21total = mi21forced + simcorrect[obslen:obslen+simlen,2]

        sim20total[:,0] = pr20total
        if nvar == 2:
            mn20forced = fmmn[1] + fmmn[0]*x20c
            mn20total = mn20forced + simmn[:obslen]
            sim20total[:,1] = mn20total
        elif nvar == 3:
            ma20forced = fmma[1] + fmma[0]*x20c
            ma20total = ma20forced + simcorrect[:obslen,1]
            mi20forced = fmmi[1] + fmmi[0]*x20c
            mi20total = mi20forced + simcorrect[:obslen,2]
            sim20total[:,1] = ma20total
            sim20total[:,2] = mi20total

# Put the simulated 21C annual values in arrays...
    sim21total[:,0] = pr21total
    if nvar == 2:
        sim21total[:,1] = mn21total
    elif nvar == 3:
        sim21total[:,1] = ma21total
        sim21total[:,2] = mi21total

# sim20total, sim21total, pr20/21forced, etc., for ma, mi -- look OK.

# Now apply k-NN, to choose the years from which to appropriate
# subannual variations. We use the non-detrended pr (ma, mi) values
# for comparison.

    obsdic = {}
    for j in range(len(obs_raw)):            # Shorter for nvar = 3, but re-
        obsdic[j] = obs_raw[j,:]             # sample from all yrs in any case

    ok = obsdic.keys()
    ok.sort()                                # (= 0,1,2,...full obslen)

# Indices of the resampled years
    jixar21 = 9999*np.ones(simlen,dtype=int)

# Number of candidate NN...
    nnn = 5                                  # Number of nearest neighbors

# Construct resampling kernel, "choice" arrays, probabilities 1/n
    pmf0 = 1./np.arange(1,nnn+1)             # Resampling kernel
    pmf  = pmf0/pmf0.sum()                   # Normalized
    cpmf = np.cumsum(pmf)                    # Partition the unit interval

    if xval == 1:
        jixar20 = 9999*np.ones(obslen,dtype=int)  # Need an integer nan
        choose20 = rand(obslen)              # Random uniform distribution
        choice20 = np.zeros(obslen,dtype=int)# Array to fill
        for j in range(obslen):              # Yields an integer, 0 - 4
            choice20[j] = int((choose20[j] - cpmf > 0.).sum())
        
    choose21 = rand(simlen)                  # Random uniform distribution
    choice21 = np.zeros(simlen,dtype=int)    # Array to fill

    for j in range(simlen):                  # Yields an integer, 0 - 4
        choice21[j] = int((choose21[j] - cpmf > 0.).sum())

# Now, construct the actual sequence of years for resampling
    if xval == 1:
        for i in range(obslen):              # Do for 20C
            dvec = rkm(obs_raw,sim20total[i,:], CI, nvar)
            dsort = np.argsort(dvec)         # Longer than 5...
            jixar20[i] = ok[dsort[choice20[i]]]

# Now, 21C...
    for i in range(simlen):                  # range(50, 66... as specified)
        dvec = rkm(obs_raw, sim21total[i,:], CI,nvar)
        dsort = np.argsort(dvec)             # Longer than 5...
        jixar21[i] = ok[dsort[choice21[i]]]

#####################################################################
#
# This completes the k-NN setup and the resampling. Now we scale the
# full gridded fields of simulated seasonal values. No more cycling
# through gridpoints (for that purpose).
# 
#####################################################################

# Start with prrecon, marecon, mirecon, the raw reconstructed fields:
# They are area-unweighted and unstandardized and pr is expressed as
# mm/d, but there are no trends and the data are not variance-matched
# to obs.

    xymask = prseas.mask[0]

# Set up arrays
    prforcedar = 1e20*np.ones((obslen+simlen,nlats,nlons))
    if nvar == 2:
        mnforcedar = 1e20*np.ones((obslen+simlen,nlats,nlons))
    elif nvar == 3:
        maforcedar = 1e20*np.ones((obslen+simlen,nlats,nlons))
        miforcedar = 1e20*np.ones((obslen+simlen,nlats,nlons))

# Compute gridbox-level forced signals, for both 20C and 21C
    prforcedtrunc = prforced[begix:endix+simlen]
    prforcedar0 = np.resize(prforcedtrunc, (nlats, obslen+simlen)).T
    for k in range(nlons):
        prforcedar[:,:,k] = prforcedar0      # Smooth, all boxes identical

# Compute observed pr trends (rel to linear, which was the detrending
# method) Then the deviation of these trends from the regional mean
# trend, and finally, area-unweight and scale these deviations and add
# them to prforcedar, to scatter the gridbox-level trends around the
# mean.

    xtrrestore = np.arange(sseas[0])         # x-variable for detrending
    xtrar0 = np.resize(xtrrestore,(nlats,obslen)).T
    xtrar = 1e20*np.ones((obslen,nlats,nlons))
    for k in range(nlons):
        xtrar[:,:,k] = xtrar0

    trrecon = xtrar*coef0[0,:,:] + coef1[0,:,:] # Looks OK, 2012-11-22
    trrmean0 = aav.av(trrecon, lats)
#    trrmean0 = trrecon.mean(axis=2).mean(axis=1)
    trrmean1 = np.resize(trrmean0, (nlats, obslen+simlen)).T
    trrmean = 1e20*np.ones((obslen+simlen,nlats,nlons))
    for k in range(nlons):
        trrmean[:,:,k] = trrmean1

    prftrunctrunc = prforced[begix:endix]
    trproject = 1e20*np.ones(prforcedar.shape)

    for i in range(nlats):
        for j in range(nlons):
            if not xymask[i,j]:
                fm = lm(prftrunctrunc,trrecon[:,i,j])
                trproject[:,i,j] = fm[0]*prforcedtrunc + fm[1]

    trprojectm = masked_greater(trproject,1e10)
    trprojmean0 = aav.av(trprojectm, lats)
    trprojmean1 = np.resize(trprojmean0,(nlats,obslen+simlen)).T
    trprojmean  = 1e20*np.ones((obslen+simlen,nlats,nlons))
    for i in range(nlons):
        trprojmean[:,:,i] = trprojmean1
    trdevs = trproject - trprojmean

    alphapr = 0.25

    print 'prf:', prforcedar.shape, prforced.shape, prseas.shape

# Now let's ignore all of the proceeding trend manipulations...
# Note: This gives almost identical results
#    for j in range(nlats):
#        for k in range(nlons):
#            fmtr = lm(pr20forced,prseas[begix:endix,j,k])
#            prforcedar[:,j,k] = fmtr[0]*prforced[begix:endix+simlen] + fmtr[1]
    
    prforcedar += alphapr*trdevs

# For ma, mi, direct projection on 21C tmmm
    xmmar0 = np.resize(xmm,(nlats, obslen+simlen)).T
    xmmar = 1e20*np.ones((obslen+simlen,nlats,nlons))
    for j in range(nlats):
        for k in range(nlons):
            xmmar[:,j,k] = xmm

    maforcedar = coef0[1]*xmmar + coef1[1]
    miforcedar = coef0[2]*xmmar + coef1[2]

# Now we move to reconstructions (cf. line 496 et seq). These will
# have to be variance-adjusted (this was done earlier but only for the
# area means; now we're operating on the full gridded fields), and
# will provide the entire a2d component of the simulation.

    yrpr = prseas[begix:endix,:,:]           # Observed (gridded) SONDJF
    yrma = maseas[begix:endix,:,:]
    yrmi = miseas[begix:endix,:,:]
    
    yrprdt = 1e20*np.ones(yrpr.shape)
    yrmadt = 1e20*np.ones(yrpr.shape)
    yrmidt = 1e20*np.ones(yrpr.shape)
    for i in range(nlats):
        for j in range(nlons):
            if not xymask[i,j]:
                yrprdt[:,i,j], line, coef = detrend2.dt(yrpr[:,i,j])
                yrmadt[:,i,j], line, coef = detrendmmm.dt(yrma[:,i,j],\
                                                   obsstartyear,'rcp45')
                yrmidt[:,i,j], line, coef = detrendmmm.dt(yrmi[:,i,j],\
                                                   obsstartyear,'rcp45')

# The section below produces first estimates of future variables,
# resolved at the grid scale. These may be useful in simgenPCA1, in
# which the k-NN is performed over space, but they are not used
# elsewhere in this script.

# Mask
    yrprdtm = masked_greater(yrprdt,1e10)
    yrmadtm = masked_greater(yrmadt,1e10)
    yrmidtm = masked_greater(yrmidt,1e10)
# Obs values
    obsprvar = yrprdt.var(axis=0)
    obsmavar = yrmadt.var(axis=0)
    obsmivar = yrmidt.var(axis=0)
# Sim values
    prreconvar = prrecon.var(axis=0)
    mareconvar = marecon.var(axis=0)
    mireconvar = mirecon.var(axis=0)
# Differences
    vardiffpr = obsprvar - prreconvar
    vardiffma = obsmavar - mareconvar
    vardiffmi = obsmivar - mireconvar
# Checking up
    negprvardiff = (vardiffpr < 0).sum()
    negmavardiff = (vardiffma < 0).sum()
    negmivardiff = (vardiffmi < 0).sum()
# Set neg values, if any, to zero
    vardiffpr[vardiffpr < 0] = 0.
    vardiffma[vardiffma < 0] = 0.
    vardiffmi[vardiffmi < 0] = 0.
# Take sqrts -> std(diffs)
    stddiffpr = np.sqrt(vardiffpr)
    stddiffma = np.sqrt(vardiffma)
    stddiffmi = np.sqrt(vardiffmi)
# Add in a dollop of noise
    scalepr = prrecon + sn(prrecon.shape)*stddiffpr
    scalema = marecon + sn(marecon.shape)*stddiffma
    scalemi = mirecon + sn(mirecon.shape)*stddiffmi
# Forced plus a2d...
    prtot = prforcedar + scalepr
    matot = maforcedar + scalema
    mitot = miforcedar + scalemi

#####################################################################
#
# prtot, matot, mitot have the correct gridbox variance and include
# the trend terms, but are resolved only at the seasonal level (i.e.,
# one value per year). Here we pick out the three or six monthly
# values for each resampled year and 'inject' the (full) simulated
# values of prtot, matot, mitot, modifying all the months in the
# seasonal window accordingly. The adjustments are additive, and
# performed at the gridbox scale.
#
#####################################################################

#####################################################################
#
# If xval ==1, process the obslen (i.e., 20C) years.
#
# Note: The case of xval == 0 was handled in the preparatory section
# of the code, for all gridpoints in one fell swoop. Complete values,
# including all months, should be present in this case, through
# (2009).
#
#####################################################################

    lax, lox = 23, 28
    
    if xval == 1:                     # We simulate 20C
        for k in range(obslen):
            jix = jixar20[k]          # 0 <= jix <= 107
            if seas == 'DJF':
                rstart = jix*12 + 11  # Into raw obs via k-NN
                rend   = rstart +  3
                start  = k*12 + 11 
                end    = start + 3
            elif seas == 'SONDJF':
                rstart = 12*jix + 8   # jix=0 -> Sep 1901-Feb 1902
                rend   = rstart + 6   # Index into monthly gridded values
                start  = 12*k + 8     # Index into "allmos" sim
                end    = start + 6

# For this version of simgenPCA we will use additive corrections for all vars
            if nvar == 2:
                mncorrect = scalemn[k] - yrmn0[jix]
            elif nvar == 3:
                """ Note: prtot is 3-D; pr20total is 1-D. """
#                prcorrect = pr20total[k] - obspr_raw[jix]     # 1-D
#                macorrect = ma20total[k] - obsma_raw[jix]
#                micorrect= mi20total[k] - obsmi_raw[jix]
                prcorrect = prtot[k] - yrpr[jix]# add to yrpr to get back prtot
                macorrect = matot[k] - yrma[jix]# grid by grid
                micorrect = mitot[k] - yrmi[jix]# by grid

# Previously we had to restrict jix to values > 0, since this value
# would summon months prior to the beginning of the data. Now instead
# we play a little trick: We shift simstartyear by -1 (early in code)
# and compute the simulations based on the segment starting a year
# early. The first year of the resulting sequence has missing values
# for JF (when simulating SONDJF, e.g.), since we can't compute a
# proper scaling factor using only two months of obs data. JF of the
# succeeding year is present, however, and when the first year is
# lopped off (near the very end of the code) we end up with a
# simulation that begins with the specified year and has no missing
# months. A similar gambit is applied at the end of the series.

# Note that we are rescaling not just a 6-month segment but a data
# slice that is (6,nlats,nlons). It may be preferable therefore to
# base the k-NN on the entire map for the simulated year, rather than
# simply the aav over the map. This has not yet been done, as of
# 2012-11-26.

# The success of this "correction" depends on the seasonal means in
# prseas, maseas, miseas matching the 6-month segments in prseg. They
# are supposed to refer to the same data, but in case of discrepancies
# between prsim and prfocedar at various gridpoints that might be the
# place to look for the problem...

            prseg = prallmos[rstart:rend,:,:] # To rescale
            if nvar == 2:
                mnseg = mnallmos[rstart:rend,i,j]
            elif nvar == 3:
                maseg = maallmos[rstart:rend,:,:]
                miseg = miallmos[rstart:rend,:,:]

            prsim[start:end] = prseg + prcorrect
            if nvar == 2:
                mnsim[start:end] = mnseg + mncorrect
            elif nvar == 3:
                masim[start:end] = maseg + macorrect
                misim[start:end] = miseg + micorrect

# Remainder of simulation (i.e., 21C). This is always computed, but
# indices differ depending on whether xval == 0 or 1.

    lpr = 12*obslen               # len(prsim of observed period)
    for k in range(simlen-1):     # range(31, 41... )
        jix = jixar21[k]          # Resampled 21C years
        if seas == 'DJF':
            rstart = jix*12 + 11  # 20c chunk to rescale
            rend = rstart + 3
            start = lpr + k*12 + 11   # ??
            end = start + 3
        elif seas == 'SONDJF':
            rstart = jix*12 + 8
            rend = rstart + 6
            start = lpr + k*12 + 8           # Consistent with other ixs? knn?
            end = start + 6

        k2 =  obslen + k          # years into 21C part of sim

        prseg  = prallmos[rstart:rend]
        prcorrect = prtot[k2] - yrpr[jix]
        if nvar == 2:
            """ This (and most other code for nvar == 2) has
            not been updated for the "PCA" version of simgen. """
            mncorrect = scalemn[k2] - yrmn0[jix]
        elif nvar == 3:
            macorrect = matot[k2] - yrma[jix]
            micorrect = mitot[k2] - yrmi[jix]
#            prcorrect = pr21total[k] - obspr_raw[jix]
#            macorrect = ma21total[k] - obsma_raw[jix]
#            micorrect = mi21total[k] - obsmi_raw[jix]
                
        prseg  = prallmos[rstart:rend,:,:]
        prsim[start:end,:,:] = prseg + prcorrect
        if nvar == 2:
            mnseg = mnallmos[rstart:rend]
            mnsim[start:end] = mnseg + mncorrect  # prseg -> prtot
        elif nvar == 3:
            maseg  = maallmos[rstart:rend,:,:]
            miseg  = miallmos[rstart:rend,:,:]
            masim[start:end,:,:] = maseg + macorrect
            misim[start:end,:,:] = miseg + micorrect

# Tmax should be greater than Tmin (according to ACRU, anyway)
    print 'Checking for ma < mi...'
    if nvar == 3:                            # If there is a Tmax...
        eps = 0.1
        diffs = misim - masim
        ixs = diffs > 0.
        if ixs.sum() > 0:
            print 'Found', ixs.sum(), 'nonconforming values, fixing...'
            masim[ixs] += diffs[ixs] + eps
        else:
            pass

# And pr must be >= 0, for obvious reasons.

    ixs = prsim < 0.
    ixsum = ixs.sum()
    if ixsum > 0:
        print 'Detected', ixsum, 'prsim values < 0, out of about 1.3M'
        print 'Attempting to correct...'
        sys.stdout.flush()
        prsim[ixs] = 0.

        ixs = prsim < 0.
        if ixs.sum() > 0:
            print 'Failed! See line 1020.'
            return
        else:
            print 'Success!'
    else:
        pass

    prsimm = masked_greater(prsim, 1e10)
    if nvar == 2:
        mnsimm = masked_greater(mnsim, 1e10)
    elif nvar == 3:
        masimm = masked_greater(masim, 1e10)
        misimm = masked_greater(misim, 1e10)

# Restore obsstartyear and take one off the end, in order to provide
# only full data years. We aim to please!

    prsimm = prsimm[12:-12]
    masimm = masimm[12:-12]
    misimm = misimm[12:-12]
    
    if write:
        writefile(lats,lons,nvar,tunitsallmos,simix,begix,endix,seas,\
                  bfactor,simlen,dirname,prsimm,masimm,misimm)
    else:
        pass

    return jixar20,jixar21,prtot,matot,mitot,prforcedar,maforcedar,miforcedar,prsimm, masimm, misimm  

    
################## End of main program #########

def writefile(lats,lons,nvar,tunitsallmos,simix,begix,endix,seas,bfactor,\
              simlen,dirname,prsimm,masimm,misimm):
    print 'Writing file...'
    begix += 1
    endix -= 1
    froot = '_TS3.1or10_25-40S_45-65W'
    taxfull = np.arange(-707.5, 1632.5)  # len = 12 * 195
    taxsim = taxfull[12*begix:12*(endix+simlen)]
    fname = dirname+'/simpre'+'_'+seas+'_%4.2f' % (bfactor)+\
            '_'+str(simix).zfill(5)+froot
    ncwrite.w({'pre':prsimm},'pre','mm/d',lats,lons,\
            taxsim, tunitsallmos, fname)
    if nvar == 2:
        fname = dirname+'/simtmp'+'_'+seas+'_%4.2f' % (bfactor)+\
                '_'+str(simix).zfill(5)+froot
        ncwrite.w({'tmp':mnsimm},'tmp','deg C', lats, lons,\
                 taxsim, tunitsallmos, fname)
    elif nvar == 3:
        fname = dirname+'/simtmx'+'_'+seas+'_%4.2f' % (bfactor)+\
                '_'+str(simix).zfill(5)+froot
        ncwrite.w({'tmx':masimm},'tmx','deg C', lats, lons,\
                 taxsim, tunitsallmos, fname)
        fname = dirname+'/simtmn'+'_'+seas+'_%4.2f' % (bfactor)+\
                '_'+str(simix).zfill(5)+froot
        ncwrite.w({'tmn':misimm},'tmn','deg C', lats, lons,\
                 taxsim, tunitsallmos, fname)

    return

#################################################

    if UY:
        latsUY = lats[10:20]                 # 30-35S
        lonsUY = lons[12:24]
        prUY = prsimm[12:,10:20,12:24]
        if nvar == 2:
            mnUY = mnsimm[12:,10:20,12:24]
        elif nvar == 3:
            maUY = masimm[12:,10:20,12:24]
            miUY = misimm[12:,10:20,12:24]
        suy = prUY.shape
        froot = seas+'_%4.2f'%(bfactor)+'_'+str(simix).zfill(5)+\
                '_TS3.1_UY.dat'
        print 'Opening UY files:'
        fnamepr = dirname+'/simpre'+froot
        print fnamepr
        fobjpr = open(fnamepr,'w')
        if nvar == 2:
            fnamemn = dirname+'/simtmp'+froot
            print fnamemn
            fobjmn = open(fnamemn,'w')
        elif nvar == 3:
            fnamema = dirname+'/simtmx'+froot
            fnamemi = dirname+'/simtmn'+froot
            print fnamema
            print fnamemi
            fobjma = open(fnamema,'w')
            fobjmi = open(fnamemi,'w')

        datelist = gendate(suy[0], obsstartyear + 1)
        firstrowlist  = ['Latitudes ']
        secondrowlist = ['Longitudes']

# First write the header rows
        print 'Writing headers...' 
        for i in range(len(latsUY)):
            for j in range(len(lonsUY)):
                if not prUY.mask[0,i,j]:
                    firstrowlist.append('%8.2f'%(latsUY[i]))
                    secondrowlist.append('%8.2f'%(lonsUY[j]))
        firstrowstr  = ' '.join(firstrowlist)+'\n'
        secondrowstr = ' '.join(secondrowlist)+'\n'
        fobjpr.write(firstrowstr)
        fobjpr.write(secondrowstr)
        if nvar == 2:
            fobjmn.write(firstrowstr)
            fobjmn.write(secondrowstr)
        elif nvar == 3:
            fobjma.write(firstrowstr)
            fobjma.write(secondrowstr)
            fobjmi.write(firstrowstr)
            fobjmi.write(secondrowstr)

# Then the data...
        print 'Writing data...'
        for k in range(suy[0]):              # One row at a time
            if k/100 == k/100.:
                print 'Writing row', k
            rowlistpr = [datelist[k]]
            if nvar == 2:
                rowlistmn = [datelist[k]]
            elif nvar == 3:
                rowlistma = [datelist[k]]
                rowlistmi = [datelist[k]]
            for i in range(len(latsUY)):
                for j in range(len(lonsUY)):
                    if not prUY.mask[0,i,j]:
                        rowlistpr.append('%8.2f'%(prUY[k,i,j]))
                        if nvar == 2:
                            rowlistmn.append('%8.2f'%(mnUY[k,i,j]))
                        elif nvar == 3:
                            rowlistma.append('%8.2f'%(maUY[k,i,j]))
                            rowlistmi.append('%8.2f'%(miUY[k,i,j]))
            rowstrpr = ' '.join(rowlistpr)+'\n'
            fobjpr.write(rowstrpr)
            if nvar == 2:
                rowstrmn = ' '.join(rowlistmn)+'\n'
                fobjmn.write(rowstrmn)
            elif nvar == 3:
                rowstrma = ' '.join(rowlistma)+'\n'
                rowstrmi = ' '.join(rowlistmi)+'\n'
                fobjma.write(rowstrma)
                fobjmi.write(rowstrmi)

        print 'Closing UY files...'
        fobjpr.close()
        if nvar == 2:
            fobjmn.close()
        elif nvar == 3:
            fobjma.close()
            fobjmi.close()

#################################################

    if nvar == 2:
        return prsimm, mnsimm, prforced[begix:endix+simlen]
    elif nvar == 3:
        return prsimm, masimm, misimm, prforced[begix:endix+simlen]

#################################################

def gendate(k, simstartyr):
    mos = ['01', '02', '03', '04', '05', '06', '07', '08',\
           '09', '10', '11', '12']
    outl = []

    if k/12 != k/12.:
        print 'Number of months not divisible by 12!'
        return 'xxxx-xxx-xxx'
    else:
        nyrs = int(k/12.)                    
        for i in range(nyrs):
            yr = simstartyr + i
            for j in range(12):
                mo = mos[j]
                ix = 12*i + j
                outl.append(str(yr)+'-'+mo+'-01')

    return outl

############# End, main routine #################
#################################################

def rkm(x,xk,CI,nvar):                       # Mahalanobis distance
    """ Compute a vector of Mahalanobis distances, for the input
    vector x, the feature vector xk and the inverse covariance matrix
    CI. Note that weight for precip is double that of the combined
    temperature variables."""
    
#    wts = np.matrix(np.diag((4./6., 1./6., 1./6.)))
#    wts = np.matrix(np.diag((1.0, 0.0, 0.0)))
#    wts = np.eye(nvar)
    if nvar == 2:
        wts = np.matrix(np.diag((2./3., 1./3.)))
    elif nvar == 3:
        wts = np.matrix(np.diag((2./3., 1./6., 1./6.)))
    xm  = np.matrix(x)                       # = Euclidean for C = I
    xkm = np.matrix(xk)
    d = xm - xkm                             # x is like (40,3), xk (1,3)
    dm = d * wts * CI * wts.T * d.T          # Row vector * CI * col vector 
    val = np.sqrt(np.diag(dm))               # Weighted distance vector

    return val

########################### T H E ### E N D #########
#####################
