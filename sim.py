# custom
import ncwrite
import simInterface as intf
from aav import av as areaAverage
import knn, trender, downsampler, detrendedVar
# builtin
from numpy import arange
from numpy.random import seed
from os.path import basename, splitext

class Sim(object):
    def __init__(self, p):
        self.loadData(p)
        
    def loadData(self, p):
        self.input = intf.In(p)
        self.output = intf.Out()
        # initialize components
        self.knnSampler = knn.Sampler(p)
        if self.input.trendMethod() == 'Linear':
            self.trender = trender.LinearGridTrender(p)
        elif self.input.trendMethod() == 'Log':
            self.trender = trender.LogGridTrender(p)
        else:
            raise Exception('sim.Sim.init: Unrecognized trend method')
        self.downsampler = downsampler.Sampler(p)
        self.variability = detrendedVar.DetrendedVariability(p)
        # load only data that are needed
        self.lats, self.lons = self.input.loadLatLonGrid()
        # default run number
        self.runNo = '000'
    
    def run(self):
        out = self.output
            
        # seed random number generator
        seed(self.input.rngSeed())
        
        # perform kNN sampling
        out.prTrReg, out.maTrReg, out.miTrReg = self.trender.regionalTrends()
        out.prVarReg, out.maVarReg, out.miVarReg, out.noiseReg, out.sqrtVarDiffReg = self.variability.regionalVariability()
        out.prTotReg = out.prTrReg + out.prVarReg
        out.maTotReg = out.maTrReg + out.maVarReg
        out.miTotReg = out.miTrReg + out.miVarReg
        out.jixar20, out.jixar21 = self.knnSampler.sample(out.prTotReg, out.maTotReg, out.miTotReg)
        
        # calculate gridbox-level trends and variabilities
        out.prTrAr, out.maTrAr, out.miTrAr = self.trender.gridboxTrends()
        out.prVarAr, out.maVarAr, out.miVarAr, out.noiseAr, out.sqrtVarDiffAr = self.variability.gridboxVariability()
        out.prTotAr = out.prTrAr + out.prVarAr
        out.maTotAr = out.maTrAr + out.maVarAr
        out.miTotAr = out.miTrAr + out.miVarAr
        
        # calculate averages
        out.prTrAve = areaAverage(out.prTrAr, self.lats)
        out.maTrAve = areaAverage(out.maTrAr, self.lats)
        out.miTrAve = areaAverage(out.miTrAr, self.lats)
        out.prVarAve = areaAverage(out.prVarAr, self.lats)
        out.maVarAve = areaAverage(out.maVarAr, self.lats)
        out.miVarAve = areaAverage(out.miVarAr, self.lats)
        out.prTotAve = out.prTrAve + out.prVarAve
        out.maTotAve = out.maTrAve + out.maVarAve
        out.miTotAve = out.miTrAve + out.miVarAve
    
        # downsample to seasonal, filling in gaps
        out.prAll, out.maAll, out.miAll = self.downsampler.sample(out.prTotAr, out.maTotAr, out.miTotAr, out.jixar20, out.jixar21)
    
        # get time in months since start of observational period
        timeUnits = u'months since %i-01-01' % self.input.obsStartYear()
        out.time = arange(0.5, 12 * (self.input.obslen() + self.input.simlen() - 2), 1)

        # write file
        if self.input.write(): self.write(out.prAll, out.maAll, out.miAll, out.time, timeUnits)
        
        # save output
        self.output = out
        
        # return same output as original
        return out.jixar20, out.jixar21, out.prTrAr, out.maTrAr, out.miTrAr, \
            out.prTotAr, out.maTotAr, out.miTotAr, out.prAll, out.maAll, out.miAll
       
    # utility function
    def write(self, pr, ma, mi, time, tunits):
        fname = self.input.dirname() + '/simall' + '_' + self.input.seasStr() \
            + '_%4.2f' % self.input.bfactor() + '_' + \
            str(self.input.simix()).zfill(5) + self.input.outputAppend() + self.runNo
        ncwrite.w({'pre' : pr, 'tmx': ma, 'tmn': mi}, \
            {'pre': 'mm/d', 'tmx': 'deg C', 'tmn': 'deg C'}, \
            self.lats, self.lons, time, tunits, self.runNo, fname)
        return

# simulation class that accepts parameter text file
class SimFile(Sim):
    def __init__(self, f = 'params_000.txt'):
        # extract parameter dictionary
        p = eval(open(f, 'r').read())
        # call superclass
        super(SimFile, self).__init__(p)
        # get simulation run number
        baseFile = basename(splitext(f)[0])
        idx = baseFile.find('_')
        if idx == -1:
            raise Exception('sim.SimFile.init: Parameter file must have run number')
        runNo = baseFile[idx + 1 :]
        if runNo == '':
            raise Exception('sim.SimFile.init: Parameter file must have run number')
        self.runNo = runNo