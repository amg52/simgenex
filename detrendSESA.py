from numpy import loadtxt
from scipy.stats import linregress as lm

def dt(pr, bfactor, startYear):
    # detrend precipitation in SESA using two trends and balance
    # factor "bfactor."
    
    # hardcoded location of trend files - for SESA only!
    prT = loadtxt('dat/prhat_tmmm_mmda_SONDJF.dat')
    prO = loadtxt('dat/prhat_O3_mmda_SONDJF.dat')
    tr0 = bfactor * prT + (1. - bfactor) * prO
    
    years = range(1901, 2012)
    
    start = years.index(startYear)
    tr = tr0[start : start + len(pr)]        # Here the records are long
                                             # enough for this to work...
    fm = lm(tr, pr)
    ln = fm[0] * tr + fm[1]
    out = pr - ln
    
    return out, ln, fm
