# custom
import simInterface as intf
from recon import r1 as rec
from aav import av as areaAverage
# builtin
from numpy import sqrt, array, ones
from numpy.random import standard_normal as sn

class DetrendedVariability(object):
    def __init__(self, p = {}):
        self.input = intf.In(p)
        # load only data that are needed
        self.pcs = self.input.loadPCs()
        self.obsSeasDtPr, self.obsSeasDtTmax, self.obsSeasDtTmin = self.input.loadGriddedDetrendedObservations()
        self.eofPr, self.eofTmax, self.eofTmin, self.meanArray, self.stdArray, self.coef0, self.coef1 = self.input.loadEOFs()
        self.lats = self.input.loadLatLonGrid()[0]
        self.obsvar = self.input.loadYearlyDetrendedObservations().var(axis = 0)
        # commonly used quantities
        self.obslen = self.input.obslen()
        self.simlen = self.input.simlen()
        self.nPCs = self.pcs.shape[1] # second dimension of array
        self.prrecon = rec(self.eofPr, self.pcs, 'pr', self.nPCs, self.meanArray, self.stdArray, self.lats)
        self.marecon = rec(self.eofTmax, self.pcs, 'tmx', self.nPCs, self.meanArray, self.stdArray, self.lats)
        self.mirecon = rec(self.eofTmin, self.pcs, 'tmn', self.nPCs, self.meanArray, self.stdArray, self.lats)
     
    def gridboxVariability(self):
        # get difference in variance between detrended observation and reconstructed data
        vardiffpr = self.obsSeasDtPr.var(axis = 0) - self.prrecon.var(axis = 0)
        vardiffma = self.obsSeasDtTmax.var(axis = 0) - self.marecon.var(axis = 0)
        vardiffmi = self.obsSeasDtTmin.var(axis = 0) - self.mirecon.var(axis = 0)
        vardiffpr[vardiffpr < 0] = 0
        vardiffma[vardiffma < 0] = 0
        vardiffmi[vardiffmi < 0] = 0
        
        # correct reconstructed data to match variance        
        noise = array(zip(sn(self.prrecon.shape), sn(self.marecon.shape), sn(self.mirecon.shape)))
        sqrtvardiff = array(zip(sqrt(vardiffpr), sqrt(vardiffma), sqrt(vardiffmi)))
        pr = self.prrecon + noise[:, 0, :, :] * sqrtvardiff[:, 0, :]
        ma = self.marecon + noise[:, 1, :, :] * sqrtvardiff[:, 1, :]
        mi = self.mirecon + noise[:, 2, :, :] * sqrtvardiff[:, 2, :]
        
        return pr, ma, mi, noise, sqrtvardiff # last two added for debug purposes
        
    def regionalVariability(self):
        # reconstruct
        simpr0 = areaAverage(self.prrecon, self.lats)
        simma0 = areaAverage(self.marecon, self.lats)
        simmi0 = areaAverage(self.mirecon, self.lats)
    
        # correct variances
        sim0 = 1e20 * ones((len(simpr0), 3))
        sim0[:, 0] = simpr0
        sim0[:, 1] = simma0
        sim0[:, 2] = simmi0
        vardiff = self.obsvar - sim0.var(axis = 0)
        for i in range(len(vardiff)):
            if vardiff[i] < 0: vardiff[i] = 0
        noise = sn(sim0.shape)
        sqrtvardiff = sqrt(vardiff)
        simcorrect = sim0 + noise * sqrtvardiff
     
        pr = simcorrect[: self.obslen + self.simlen, 0]
        ma = simcorrect[: self.obslen + self.simlen, 1]
        mi = simcorrect[: self.obslen + self.simlen, 2]
        
        return pr, ma, mi, noise, sqrtvardiff # last two added for debug purposes