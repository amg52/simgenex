# import and call startup script
import startup
startup

# custom
from createMap import createMap
# builtin
import cPickle as pickle
from pylab import savefig
import numpy.ma as npMasked
import matplotlib.pyplot as plt
from numpy import arange, linspace

# config file
configFile = 'params/offline/WA_000.txt'

# load config file and get config file parameters
config = eval(open(configFile, 'r').read())
startYear = config['obsStartYear']
nEOFs = config['nEOFs']
nPCs = config['nPCs']
nSamples = config['nSamples']

# load data
pr, tmax, tmin = pickle.load(open(config['outputObsData']))
lats, lons = pickle.load(open(config['outputLatlonGrid']))
eofPr, eofTmax, eofTmin, eigen, vf, pcs, meanArray, stdArray, coeff0, coeff1 = pickle.load(open(config['outputEOFs'], 'r'))
estimates = pickle.load(open(config['outputRModel'], 'r'))[1]
reconData, simN = pickle.load(open(config['outputSimulatedData'], 'r'))

# ==============

# extract variables
numAverages = len(simN)
totalYears = pr.shape[0]
obsYears = arange(startYear, startYear + totalYears)

# variable names
var = ['$P$ (mm/mon)', '$T_{max}$ ($^\circ$C)', '$T_{min}$ ($^\circ$C)'] 

# close all existing figures
plt.close('all')

# ==============

# plot PCs
plt.figure()
plt.plot(obsYears, pcs)
plt.xlabel('Year')
plt.ylabel('Principal Components')
plt.xlim(startYear, startYear + totalYears)
plt.xticks(range(startYear, startYear + totalYears, 20))
plt.show()
savefig('fig1.png')

# plot first five EOFs
nEOFsToPlot = 5 if nEOFs >= 5 else nEOFs
fig, axes = plt.subplots(nrows = nEOFsToPlot, ncols = 3, figsize = (10, 10))
for i in range(axes.shape[0]):
    for j in range(axes.shape[1]):
        m = createMap(axes[i, j], lats, lons)
        if i == 0 and j == 0:
            # only perform once
            glons, glats = m.makegrid(len(lons), len(lats))
            x, y = m(glons, glats)
        plotData = eofPr if j == 0 else (eofTmax if j == 1 else eofTmin)
        cLower = npMasked.min(plotData)
        cUpper = npMasked.max(plotData)
        cs = m.contourf(x, y, plotData[i], linspace(cLower, cUpper, 100))
        cbar = m.colorbar(cs, location = 'right', format = '%.3f', ticks = linspace(cLower, cUpper, 3)) 
        axes[i, j].set_title(var[j] + ' EOF ' + str(i + 1))
fig.tight_layout()
fig.show()
savefig('fig2.png')

# plot mean of variables
# (should be close to zero because detrended)
fig, axes = plt.subplots(nrows = 3, ncols = 1, figsize = (10, 10))
for i in range(len(axes)):
    m = createMap(axes[i], lats, lons)
    if i == 0:
        # perform only once
        glons, glats = m.makegrid(len(lons), len(lats))
        x, y = m(glons, glats)
    cs = m.contourf(x, y, meanArray[i])
    cbar = m.colorbar(cs, location = 'right', format = '%.3e') 
    axes[i].set_title(var[i] + ' $\mu$')
fig.tight_layout()
fig.show()
savefig('fig3.png')

# plot standard deviation of variables
fig, axes = plt.subplots(nrows = 3, ncols = 1, figsize = (10, 10))
for i in range(len(axes)):
    m = createMap(axes[i], lats, lons)
    if i == 0:
        # perform only once
        glons, glats = m.makegrid(len(lons), len(lats))
        x, y = m(glons, glats)
    cs = m.contourf(x, y, stdArray[i])
    cbar = m.colorbar(cs, location = 'right', format = '%.3f') 
    axes[i].set_title(var[i] + ' $\sigma$')
fig.tight_layout()
fig.show()
savefig('fig4.png')

# plot data and estimates
fig, axes = plt.subplots(nrows = nPCs, ncols = 1, figsize = (10, 10))
for i in range(len(axes)):
    axes[i].plot(obsYears, pcs[:, i], 'b.-', label = 'Obs')
    axes[i].plot(obsYears, estimates[:, i], 'rx-', label = 'Est')
    axes[i].set_xlabel('Year')
    axes[i].set_ylabel('PC ' + str(i + 1))
    axes[i].set_xlim(startYear, startYear + totalYears)
    axes[i].set_xticks(range(startYear, startYear + totalYears, 20))
    yticks = linspace(axes[i].get_ylim()[0], axes[i].get_ylim()[1], 4)
    axes[i].set_yticks(yticks)
    axes[i].set_yticklabels(['%.2f' % e for e in yticks])
    axes[i].legend()
fig.tight_layout()
fig.show()
savefig('fig5.png')

# plot reconstructed data and sliding window average
fig, axes = plt.subplots(nrows = 3, ncols = 1, figsize = (10, 10))
for i in range(len(axes)):
    axes[i].plot(range(nSamples), reconData[:, i], 'b', label = 'Recon')
    axes[i].plot(range(numAverages), simN[:, i], 'r', label = 'Avg')
    axes[i].set_ylabel(var[i], multialignment = 'center')
    axes[i].set_xlim(0, nSamples)
    xticks = [round(e) for e in linspace(0, nSamples, 10)]
    axes[i].set_xticks(xticks)
    axes[i].set_xticklabels(['%d' % e for e in xticks])
    axes[i].legend()
axes[-1].set_xlabel('Simulated Year')
fig.tight_layout()
fig.show()
savefig('fig6.png')