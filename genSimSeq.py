# import and call startup script
import startup
startup

# custom
import recon
from aav import av as areaAverage
import offlineProcessingFunctions as opf
# builtin
from os import remove
import cPickle as pickle
import numpy.ma as npMasked
import rpy2.robjects as robjects
from numpy import array, savetxt, loadtxt, zeros, where

# *must be run after preprocessDatafiles.process*
def sim(configFile):
    # get config file
    config = eval(open(configFile, 'r').read())
    
    # get config file parameters
    obsStartYear = config['obsStartYear']
    obsEndYear = config['obsEndYear']
    nEOFs = config['nEOFs']
    nPCs = config['nPCs']
    nSamples = config['nSamples']
    nYears = config['nYears']
    bfactor = config['bfactor']
    sres = config['sres']
    
    # ======
    # PRELIM
    # ======
    
    # preliminary error checking
    if nPCs > nEOFs: raise Exception('genSimSeq.sim: Number of PCs to save must be no greater than number of total EOFs')
    if nSamples < nYears: raise Exception('genSimSeq.sim: Number of years over which to average cannot be greater than number of years')

    # load data and extract variables
    pr, tmax, tmin = pickle.load(open(config['totalGriddedObsFile']))
    lats, lons = pickle.load(open(config['latlonGridFile']))
    years = pickle.load(open(config['yearsFile']))
    
    # select years of interest
    if obsStartYear < min(years):
        raise Exception('genSimSiq.sim: Start date out of range')
    if obsEndYear > max(years):
        raise Exception('genSimSeq.sim: End date out of range')
    startIdx = where(years == obsStartYear)[0]
    if not startIdx.size:
        raise Exception('genSimSeq.sim: Start year not found')
    endIdx = where(years == obsEndYear)[0]
    if not endIdx.size:
        raise Exception('genSimSeq.sim: End year not found')
    pr = pr[startIdx[0] : endIdx[0] + 1]
    tmax = tmax[startIdx[0] : endIdx[0] + 1]
    tmin = tmin[startIdx[0] : endIdx[0] + 1]
    
    # ======
    # STEP 1
    # ======
    
    # perform EOF analysis
    print 'Calculating EOFs . . .'
    # assumes pcs = 0, stdz = 1
    (eofPr,eofTmax,eofTmin,eigen,vf,pcs,meanArray,stdArray,coeff0,coeff1) = \
        opf.calcEOFs(pr,tmax,nEOFs,lats,arr2=tmin,dt=config['dt'],\
                     yr0=obsStartYear,bfactor=bfactor,sres=sres)
    
    # save PCs to file
    if nPCs > pcs.shape[1]: raise Exception('genSimSeq.sim: Selected too many PCs')
    pcsTempFile = 'pcs_temp.txt'
    savetxt(pcsTempFile, pcs[:, : nPCs])
    
    # ======
    # STEP 2
    # ======
    
    # define R functions
    robjects.r('''
        loadData <- function(pcsFile) {
            # read data from file
            data <- read.table(pcsFile)
            return(data)
        }
        createModel <- function(data) {
            # estimate VAR TSmodel from data
            library(dse)
            tsdat <- TSdata(output = data)
            mdl <- estVARXls(tsdat, max.lag = 1)
            return(mdl)
        }
        simulatePCs <- function(mdl, simFile, nSamples, rngSeed) {
            # simulate from model
            library(dse)
            set.seed(rngSeed)
            sim <- simulate(mdl, sampleT = nSamples)
            write(t(sim$output), file = simFile, ncolumns = dim(mdl$data$output)[2])
        }
        ''')
    
    # load R functions
    r_loadData = robjects.globalenv['loadData']
    r_createModel = robjects.globalenv['createModel']
    r_simulatePCs = robjects.globalenv['simulatePCs']
    
    # run R functions to generate simulated data
    print 'Creating model and running simulation . . .'
    r_pcs = r_loadData(pcsTempFile)
    r_mdl = r_createModel(r_pcs)
    r_simulatePCs(r_mdl, config['pcsFile'], nSamples, config['rngSeed'])
    remove(pcsTempFile) # delete file
    
    # ======
    # STEP 3
    # ======
    
    # reconstruct data and area average
    print 'Reconstructing data . . .'
    simulatedPCs = loadtxt(config['pcsFile'])
    reconData = npMasked.empty([nSamples, 3])
    reconPr = recon.r1(eofPr, simulatedPCs, 'pr', nPCs, meanArray, stdArray, lats)
    reconData[:, 0] = areaAverage(reconPr, lats)
    reconTmax = recon.r1(eofTmax, simulatedPCs, 'tmx', nPCs, meanArray, stdArray, lats)
    reconData[:, 1] = areaAverage(reconTmax, lats)
    reconTmin = recon.r1(eofTmin, simulatedPCs, 'tmn', nPCs, meanArray, stdArray, lats)
    reconData[:, 2] = areaAverage(reconTmin, lats)
    
    # perform sliding window average
    print 'Calculating running average . . .'
    numAverages = nSamples - nYears + 1
    simN = zeros((numAverages, 3))
    for i in range(numAverages): simN[i, :] = reconData[i : i + nYears, :].mean(axis = 0)

    # find candidate years
    print 'Searching for candidates . . .'
    candidates = opf.qsearch(simN, 'pr', config['prPercentile'], config['prEpsilon'], config['tempEpsilon'])
    
    # save EOFs and others
    print 'Saving EOFs . . .'
    pickle.dump([eofPr, eofTmax, eofTmin, eigen, vf, pcs, meanArray, stdArray, coeff0, coeff1], open(config['eofsFile'], 'w'))
        
    # save candidates
    print 'Saving candidates . . .'
    pickle.dump(candidates, open(config['candidatesFile'], 'w'))
    
    # save R model
    print 'Saving R model . . .'
    estimates = array(robjects.r('%s$estimates' %(r_mdl.r_repr()))[2])
    pickle.dump([r_mdl, estimates], open(config['rModelFile'], 'w'))
    
    # save simulated data
    print 'Saving simulated data . . .'
    pickle.dump([reconData, simN], open(config['simulatedDataFile'], 'w'))
