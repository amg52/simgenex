# custom
import simInterface as intf
from mahalanobis import rkm
# builtin
from numpy.random import rand
from numpy import matrix, cov, ones, zeros, arange, cumsum, argsort

class Sampler(object):
    def __init__(self, p = {}):
        self.input = intf.In(p)
        # load only data that are needed
        self.raw = self.input.loadYearlyObservations()
        # commonly used quantities
        self.obslen = self.input.obslen()
        self.simlen = self.input.simlen()
        
    def sample(self, pr, ma, mi):
        # apply kNN sampling to choose years from which to appropriate subannual variations
        obsdic = {}
        for j in range(len(self.raw)):
            obsdic[j] = self.raw[j, :]
        ok = obsdic.keys()
        ok.sort()
    
        # indices of resampled years
        jixar20 = []
        jixar21 = 9999 * ones(self.simlen, dtype = int)
    
        # construct resampling kernel, "choice" arrays, probabilities 1/n
        pmf = 1. / arange(1, self.input.nnn() + 1)
        pmf /= pmf.sum()
        cpmf = cumsum(pmf)

        if self.input.xval():
            choose20 = rand(self.obslen)
            choice20 = zeros(self.obslen, dtype = int)
            for j in range(self.obslen): choice20[j] = int((choose20[j] - cpmf > 0.).sum())
        choose21 = rand(self.simlen)
        choice21 = zeros(self.simlen, dtype = int)
        for j in range(self.simlen): choice21[j] = int((choose21[j] - cpmf > 0.).sum())
    
        # construct actual sequence of years for resampling
        CI = matrix(cov(self.raw.T)).I
        if self.input.xval():
            jixar20 = 9999 * ones(self.obslen, dtype = int)
            for i in range(self.obslen):
                dvec = rkm(self.raw, [pr[i], ma[i], mi[i]], CI, 3)
                dsort = argsort(dvec)
                jixar20[i] = ok[dsort[choice20[i]]]
        for i in range(self.simlen):
            dvec = rkm(self.raw, [pr[self.obslen + i], ma[self.obslen + i], mi[self.obslen + i]], CI, 3)
            dsort = argsort(dvec)
            jixar21[i] = ok[dsort[choice21[i]]]
            
        return jixar20, jixar21