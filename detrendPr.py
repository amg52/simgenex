from numpy import loadtxt
from scipy.stats import linregress as lm

def dt(pr, startYear):
    # detrend precipitation in West Africa using trend contained in text file
    
    # hardcoded location of trend file
    tr = loadtxt('dat/WAtrend.txt')
    years = range(1901, 2012)
    
    start = years.index(startYear)
    tr = tr[start : start + len(pr)]
    
    fm = lm(tr, pr)
    ln = fm[0] * tr + fm[1]
    out = pr - ln
    
    return out, ln, fm