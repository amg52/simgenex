# custom
import simInterface as intf
# builtin
from numpy.ma import masked_greater
 
class Sampler(object):
    def __init__(self, p = {}):
        self.input = intf.In(p)
        # load only data that are needed
        self.prAll, self.tmaxAll, self.tminAll, self.prSim, self.tmaxSim, self.tminSim = self.input.loadClimatology()
        self.obsSeasPr, self.obsSeasTmax, self.obsSeasTmin = self.input.loadGriddedObservations()
        # commonly used quantities
        self.obslen = self.input.obslen()
        self.simlen = self.input.simlen()
 
    def sample(self, prtot, matot, mitot, jixar20, jixar21): 
        prSim = self.prSim
        tmaxSim = self.tmaxSim
        tminSim = self.tminSim
        
        nmos = len(self.input.seas())
        firstMonIdx = intf.In.__mths__[self.input.seas()[0]]
        if self.input.xval():
            for k in range(self.obslen):
                jix = jixar20[k]
                rstart = 12 * jix + firstMonIdx
                rend = rstart + nmos
                start = 12 * k + firstMonIdx
                end = start + nmos
                prseg = self.prAll[rstart : rend] # to rescale
                maseg = self.tmaxAll[rstart : rend]
                miseg = self.tminAll[rstart : rend]
                prSim[start : end] = prseg + (prtot[k] - self.obsSeasPr[jix])
                tmaxSim[start : end] = maseg + (matot[k] - self.obsSeasTmax[jix])
                tminSim[start : end] = miseg + (mitot[k] - self.obsSeasTmin[jix])
        
        # remainder of simulation
        for k in range(self.simlen - 1):
            jix = jixar21[k]
            rstart = 12 * jix + firstMonIdx
            rend = rstart + nmos
            start = 12 * (k + self.obslen) + firstMonIdx
            end = start + nmos
            prseg = self.prAll[rstart : rend]
            maseg = self.tmaxAll[rstart : rend]
            miseg = self.tminAll[rstart : rend]
            prSim[start : end] = prseg + (prtot[k + self.obslen] - self.obsSeasPr[jix])
            tmaxSim[start : end] = maseg + (matot[k + self.obslen] - self.obsSeasTmax[jix])
            tminSim[start : end] = miseg + (mitot[k + self.obslen] - self.obsSeasTmin[jix])
            
        # check for anomalies
        prSim, tmaxSim, tminSim = self.checkSim(prSim, tmaxSim, tminSim)
        
        prSim = masked_greater(prSim, 1e10)
        tmaxSim = masked_greater(tmaxSim, 1e10)
        tminSim = masked_greater(tminSim, 1e10)
    
        # restore obsstartyear and take one off end in order to provide only full data years
        prSim = prSim[12 : -12]
        tmaxSim = tmaxSim[12 : -12]
        tminSim = tminSim[12 : -12]
        
        return prSim, tmaxSim, tminSim
       
    # utility function 
    def checkSim(self, pr, ma, mi, eps = 0.1):
        # ensure tmax > tmin
        diffs = mi - ma
        ixs = diffs > 0.
        if ixs.sum() > 0: ma[ixs] += diffs[ixs] + eps
        # ensure pr >= 0
        ixs = pr < 0.
        if ixs.sum() > 0:
            pr[ixs] = 0.
            if (pr < 0.).sum() > 0:
                print 'downsampler.Sampler.checkSim: Failed!'
                return             
        return pr, ma, mi