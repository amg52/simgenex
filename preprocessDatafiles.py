# custom
from aav import av as areaAverage
import detrend2, detrendmmm, detrendPr, detrendSESA
# builtin
import cdutil, cdtime, cdms2 as cdms
from numpy import ma
import cPickle as pickle
from numpy import zeros, savetxt
from netCDF4 import Dataset as nc
from os.path import split

# helper functions
def fix(dat):
    s = dat.shape
    if len(s) == 1:
        dat.getAxis(0).designateTime()
    elif len(s) == 2:
        dat.getAxis(0).designateLatitude()
        dat.getAxis(1).designateLongitude()
    elif len(s) == 3:
        dat.getAxis(0).designateTime()
        dat.getAxis(1).designateLatitude()
        dat.getAxis(2).designateLongitude()
    elif len(s) == 4:
        dat.getAxis(0).designateTime()
        dat.getAxis(1).designateLevel()
        dat.getAxis(2).designateLatitude()
        dat.getAxis(3).designateLongitude()
    return dat

def tbfix(var):
    tax = var.getTime()
    tb = tax.genGenericBounds()
    tax.setBounds(tb)
    return var

def transientToMasked(array): return ma.array(array.getValue(), mask = array.mask)

def writeNetCDF(f, data, name):
    # create dimensions
    f.createDimension('time', data.shape[0])
    f.createDimension('lat', data.shape[1])
    f.createDimension('lon', data.shape[2])
    # set time
    time = f.createVariable('time', 'f8', ('time',))
    time.units = data.getTime().units
    time[:] = data.getTime()[:]
    # set latitude
    lat = f.createVariable('lat', 'f4', ('lat',))
    lat.units  = 'degrees north'
    lat[:]  = data.getLatitude()[:]
    # set longitude
    lon = f.createVariable('lon', 'f4', ('lon',))
    lon.units = 'degrees east'
    lon[:] = data.getLongitude()[:]
    # set variable
    var = f.createVariable(name, 'f4', ('time', 'lat', 'lon',))
    var[:] = transientToMasked(data)
    return f
    
def getYears(data):
    tax = data.getTime()
    years = zeros((len(tax),))
    for i in range(len(tax)): years[i] = cdtime.reltime(tax[i], tax.units).tocomp().year
    return years
    
def detrend(data, bfactor, startYear, sres, method):
    if method == 'None':
        return data
    if method == 'Linear':
        return detrend2.dt(data)[0]
    elif method == 'MMM':
        return detrendmmm.dt(data, startYear, sres)[0]
    elif method == 'WAPrecip':
        return detrendPr.dt(data, startYear)[0]
    elif method == 'SESAPrecip':
        return detrendSESA.dt(data, bfactor, startYear)[0]
    else:
        raise Exception('preprocessDatafiles.detrend: Unrecognized detrend method')

# main process function
def process(configFile):
    config = eval(open(configFile, 'r').read())
    
    # error checking
    if config['latUpper'] <= config['latLower']:
        raise Exception('preprocessDatafiles.process: Upper latitude must be greater than lower latitude')
    if config['lonUpper'] <= config['lonLower']:
        raise Exception('preprocessDatafiles.process: Upper longitude must be greater than lower longitude')
    if len(config['dt']) < 3:
        raise Exception('preprocessDatafiles.process: Size of dt must be three')
        
    # get config file parameters
    latLower = config['latLower']
    latUpper = config['latUpper']
    lonLower = config['lonLower']
    lonUpper = config['lonUpper']
    prDt = config['dt'][0]
    sres = config['sres']
    bfactor = config['bfactor']
    tmaxDt = config['dt'][1]
    tminDt = config['dt'][2]

    # get season object
    season = cdutil.times.Seasons(config['mths'])
    mthLabels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

    # =============
    # GENERATE DATA
    # =============

    # select region and seasonalize
    if config['mths'] == 'SONDJF':
        offset = 1
    else:
        offset = 0
    # precip
    print 'Opening precipitation file', split(config['rawPrFile'])[-1]
    fPr = cdms.open(config['rawPrFile'])
    pr = tbfix(fix(fPr('pre')))
    print 'pr units are', pr.units
    if pr.units == 'mm':
        print 'Changing to mm/d...'
        pr /= 30.                            # To mm/d
#    pr = pr(lats = (latLower, latUpper), lons = (lonLower, lonUpper))
    seasPr = season(pr)[offset:,:,:]
    # tmax
    print 'Opening maximum temperature file', split(config['rawTmaxFile'])[-1]
    fTmax = cdms.open(config['rawTmaxFile'])
    tmax = tbfix(fix(fTmax('tmx')))
#    tmax = tmax(lats = (latLower, latUpper), lons = (lonLower, lonUpper))
    seasTmax = season(tmax)[offset:,:,:]
    # tmin
    print 'Opening minimum temperature file', split(config['rawTminFile'])[-1]
    fTmin = cdms.open(config['rawTminFile'])
    tmin = tbfix(fix(fTmin('tmn')))
#    tmin = tmin(lats = (latLower, latUpper), lons = (lonLower, lonUpper))
    seasTmin = season(tmin)[offset:,:,:]
    
    # get latitude and longitude
    lats = pr.getLatitude()[:]
    lons = pr.getLongitude()[:]
    
    # get years
    years = getYears(seasPr)
    start_year = min(years)
    
    # area average and detrend
    # precip
    print 'Area averaging and detrending . . .'
    seasPrMsk = transientToMasked(seasPr) # convert to masked array
    aveSeasPrMsk = areaAverage(seasPrMsk, lats)
    avedtSeasPrMsk = detrend(aveSeasPrMsk, bfactor, start_year, sres, prDt)
    # tmax
    seasTmaxMsk = transientToMasked(seasTmax) # convert to masked array
    aveSeasTmaxMsk = areaAverage(seasTmaxMsk, lats)
    avedtSeasTmaxMsk = detrend(aveSeasTmaxMsk,bfactor,start_year,sres,tmaxDt)
    # tmin
    seasTminMsk = transientToMasked(seasTmin) # convert to masked array
    aveSeasTminMsk = areaAverage(seasTminMsk, lats)
    avedtSeasTminMsk = detrend(aveSeasTminMsk,bfactor,start_year,sres,tminDt)
    
    # detrend gridded data
    print 'Detrending gridded data . . .'
    seasdtPrMsk = seasPrMsk.copy()
    seasdtTmaxMsk = seasTmaxMsk.copy()
    seasdtTminMsk = seasTminMsk.copy()
    for i in range(len(lats)):
        for j in range(len(lons)):
            if not seasdtPrMsk.mask[0][i, j]:
                """ Don't we need to save these coeffs for Tmax, Tmin,
                to project them forward at the grid level? """
                seasdtPrMsk[:, i, j] = detrend(seasPrMsk[:, i, j], bfactor,\
                                               start_year, sres, prDt)
                seasdtTmaxMsk[:, i, j] = detrend(seasTmaxMsk[:, i, j],\
                                    bfactor, start_year, sres, tmaxDt)
                seasdtTminMsk[:, i, j] = detrend(seasTminMsk[:, i, j],\
                                    bfactor, start_year, sres, tminDt)

    # produce climatology dictionaries
    print 'Producing climatology dictionaries . . .'
    # precip
    climPr = cdutil.ANNUALCYCLE.climatology(pr)
    climPrMsk = transientToMasked(climPr) # convert to masked array
    prDic = {}
    for i in range(len(mthLabels)): prDic[mthLabels[i]] = climPrMsk[i]
    # tmax
    climTmax = cdutil.ANNUALCYCLE.climatology(tmax)
    climTmaxMsk = transientToMasked(climTmax) # convert to masked array
    tmaxDic = {}
    for i in range(len(mthLabels)): tmaxDic[mthLabels[i]] = climTmaxMsk[i]
    # tmin
    climTmin = cdutil.ANNUALCYCLE.climatology(tmin)
    climTminMsk = transientToMasked(climTmin) # convert to masked array
    tminDic = {}
    for i in range(len(mthLabels)): tminDic[mthLabels[i]] = climTminMsk[i]

    # =========
    # SAVE DATA
    # =========

    aveData = zeros((len(aveSeasPrMsk), 3))
    aveData[:, 0] = aveSeasPrMsk
    aveData[:, 1] = aveSeasTmaxMsk
    aveData[:, 2] = aveSeasTminMsk
    avedtData = zeros((len(aveSeasPrMsk), 3))
    avedtData[:, 0] = avedtSeasPrMsk
    avedtData[:, 1] = avedtSeasTmaxMsk
    avedtData[:, 2] = avedtSeasTminMsk

    print 'Writing files . . .'
    # save to pickle files
    pickle.dump([seasPrMsk, seasTmaxMsk, seasTminMsk],\
                open(config['totalGriddedObsFile'], 'w'))
    pickle.dump([seasdtPrMsk, seasdtTmaxMsk, seasdtTminMsk],\
                open(config['totalGriddedDetrendedObsFile'], 'w'))
    pickle.dump(prDic, open(config['prClimDicFile'], 'w'))
    pickle.dump(tmaxDic, open(config['tmaxClimDicFile'], 'w'))
    pickle.dump(tminDic, open(config['tminClimDicFile'], 'w'))
    pickle.dump([lats, lons], open(config['latlonGridFile'], 'w'))
    pickle.dump(years, open(config['yearsFile'], 'w'))
    # save to text files
    savetxt(config['totalYearlyObsFile'], aveData)
    savetxt(config['totalYearlyDetrendedObsFile'], avedtData)
    # save to NetCDF files
    ncPr = nc(config['prAllMosFile'], 'w', format = 'NETCDF3_CLASSIC')
    ncPr = writeNetCDF(ncPr, pr, 'pr')
    ncPr.close()
    ncTmax = nc(config['tmaxAllMosFile'], 'w', format = 'NETCDF3_CLASSIC')
    ncTmax = writeNetCDF(ncTmax, tmax, 'tmx')
    ncTmax.close()
    ncTmin = nc(config['tminAllMosFile'], 'w', format = 'NETCDF3_CLASSIC')
    ncTmin = writeNetCDF(ncTmin, tmin, 'tmn')
    ncTmin.close()

    # close files
    fPr.close()
    fTmax.close()
    fTmin.close()

##############
