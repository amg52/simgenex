"""

pr, Tmax, Tmin plots for simulation experimenting...

Usage: plot3.p(x,yr0,var,ints,lty='k-'), where

x is the matrix of (three) time series,
yr0 is the starting year (for the plot),
var is the criterion variable name (for plotting the zero line),
ints is a... tuple? y-axis intervals for the three subplots, and
lty is line specifier for the plot (like 'b-')

We expect the three cols to be pr, Tmax, Tmin, in that order

"""

import numpy as np
from pylab import arange, axis, ceil, floor, grid, linspace, plot, show, subplot, title, xlabel, yticks

def rangecomp(dat,padfrac):
    ranges = []
    for i in range(3):
        ts = dat[:,i]
        eps = padfrac*(ts.max()-ts.min())
        print 'range, pad, :',ts.max()-ts.min(),padfrac*(ts.max()-ts.min())
        print 'eps, limits:', eps, (ts.min()-eps, ts.max()+eps)
        ranges.append((ts.min()-eps, ts.max()+eps))
    return ranges

def p(z,ix,fmat,yr0,var,ints,lty='k-'):
    pr = z[:,0]; ma = z[:,1]; mi = z[:,2]
    l = len(pr)
    print 'l is', l
    if l > 200:
        off = 5
    elif 200 > l >= 100:
        off = 2
    elif l < 100:
        off = 1

    x = np.arange(yr0,yr0+l)
    titles = ('pr','Tmax','Tmin')
    ranges = rangecomp(z+fmat,0.1)
    print 'ranges:', ranges
    
    ylocs = {}
    for i in range(3):
        ylocs[i] = arange(floor(ranges[i][0]),ceil(ranges[i][1]),ints[i])

    ylabs = {}
    for i in range(3):
        ylabs[i] = []
        for j in ylocs[i]:
            ylabs[i].append('%2.1f' % j)

    ixdic = {'pr':0, 'ma':1, 'mi':2}
    if var is not None:
        varix = ixdic[var]
    else:
        varix = -1
        
    print 'varix:', varix
    xs0 = 2041*np.ones(100)
    xs1 = 2050*np.ones(100)
    if yr0 == 2000:
        ys = linspace(100,120,100)          # Data-dependent
    else:
        ys  = linspace(100,120,100)

    for i in range(3):
        subplot(3,1,i+1),plot(x,fmat[:,i] + z[:,i],lty)
        if i == varix:
            if yr0 == 2000:
                plot(x,fmat[:,varix],'k--')
            else:
                plot(x[50:],fmat[50:,varix],'k--')
                
            plot(xs0,ys,'r-',xs1,ys,'r-',lw=2)

        yticks(ylocs[i],ylabs[i])
        #axis([yr0-off,yr0+l+off-1,ranges[i][0],ranges[i][1]])
        grid()
        title(titles[i])

    if ix is not None:
        xlabel(str(ix))
        
    show()